-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 08 Jan 2017 pada 21.13
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digitalreportpoliteknik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `department`
--

CREATE TABLE `department` (
  `id` int(2) NOT NULL,
  `name_department` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `department`
--

INSERT INTO `department` (`id`, `name_department`) VALUES
(1, 'Teknik Informatika'),
(2, 'Akuntansi'),
(3, 'Mesin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_category`
--

CREATE TABLE `project_category` (
  `id_category` int(3) NOT NULL,
  `name_category` varchar(30) NOT NULL,
  `desc_category` text NOT NULL,
  `text_button_category` varchar(30) NOT NULL,
  `color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_category`
--

INSERT INTO `project_category` (`id_category`, `name_category`, `desc_category`, `text_button_category`, `color`) VALUES
(1, 'Laporan Proyek 1', 'Laporan ini sebagai bukti jika Anda sudah mengerjakan Proyek 1', 'Ayo buat Laporan Proyek 1', 'cyan'),
(2, 'Laporan Proyek 2', 'Laporan ini sebagai bukti jika Anda sudah mengerjakan Proyek 2', 'Ayo buat Laporan Proyek 2', 'pink'),
(3, 'Laporan OJT', 'Laporan ini sebagai bukti jika Anda sudah mengikuti OJT', 'Ayo buat Laporan OJT', 'green'),
(4, 'Laporan Proyek Akhir', 'Laporan ini sebagai bukti jika Anda sudah mengerjakan Proyek Akhir', 'Ayo buat Laporan Proyek Akhir', 'indigo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_file`
--

CREATE TABLE `project_file` (
  `id_file` int(5) NOT NULL,
  `id_project` int(7) NOT NULL,
  `parent` int(10) DEFAULT NULL,
  `name_file` longblob NOT NULL,
  `contents` longblob,
  `date_create` varchar(30) NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_modified_date` varchar(30) NOT NULL,
  `last_modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_file`
--

INSERT INTO `project_file` (`id_file`, `id_project`, `parent`, `name_file`, `contents`, `date_create`, `created_by`, `last_modified_date`, `last_modified_by`) VALUES
(1, 28, 1, 0x54657374, 0x3c703e3c666f6e7420636f6c6f723d2223666630303030223e6173646173646173643c2f666f6e743e3c2f703e, '23 Dec 2016 - 03:03 am', 10101010, '23 Dec 2016 - 03:05 am', 88888888);

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_folder`
--

CREATE TABLE `project_folder` (
  `id_folder` int(7) NOT NULL,
  `id_project` int(7) NOT NULL,
  `name_folder` varchar(40) NOT NULL,
  `date_create` text NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_folder`
--

INSERT INTO `project_folder` (`id_folder`, `id_project`, `name_folder`, `date_create`, `created_by`) VALUES
(1, 28, 'Pendahuluan', '22 Dec 2016 - 03:45 am', 10101010);

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_group`
--

CREATE TABLE `project_group` (
  `id` int(6) NOT NULL,
  `id_project` int(6) NOT NULL,
  `id_account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_group`
--

INSERT INTO `project_group` (`id`, `id_project`, `id_account`) VALUES
(6, 28, 15010075);

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_guide`
--

CREATE TABLE `project_guide` (
  `id` int(6) NOT NULL,
  `id_project` int(6) NOT NULL,
  `id_account` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_guide`
--

INSERT INTO `project_guide` (`id`, `id_project`, `id_account`) VALUES
(3, 28, 88888888);

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_master`
--

CREATE TABLE `project_master` (
  `id` int(11) NOT NULL,
  `project_id_category` int(6) NOT NULL,
  `project_id_owner` int(11) NOT NULL,
  `project_title` text NOT NULL,
  `project_date_create` varchar(30) NOT NULL,
  `project_date_released` varchar(20) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_master`
--

INSERT INTO `project_master` (`id`, `project_id_category`, `project_id_owner`, `project_title`, `project_date_create`, `project_date_released`, `status`) VALUES
(28, 1, 10101010, 'Test', '21 Dec 2016 - 03:46 pm', '28 Dec 2016 - 03:41:', 'PUBLIC'),
(29, 1, 10101010, 'Test', '06 Jan 2017 - 02:45 am', NULL, 'PRIVATE');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_notification_file`
--

CREATE TABLE `project_notification_file` (
  `id_notification_file` int(6) NOT NULL,
  `id_file` int(6) NOT NULL,
  `sender_notification_file` int(11) NOT NULL,
  `receiver_notification_file` int(11) NOT NULL,
  `contents_notification_file` text,
  `date_notification_file` varchar(30) NOT NULL,
  `status_notification_file` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_notification_file`
--

INSERT INTO `project_notification_file` (`id_notification_file`, `id_file`, `sender_notification_file`, `receiver_notification_file`, `contents_notification_file`, `date_notification_file`, `status_notification_file`) VALUES
(1, 1, 15010086, 88888888, '<p>Test</p>', '11 Dec 2016 - 01:20 pm', 'READ'),
(2, 2, 15010075, 88888888, '<p>Test 2</p>', '11 Dec 2016 - 01:22 pm', 'READ'),
(3, 2, 88888888, 15010075, '<p>Oke</p>', '11 Dec 2016 - 01:22 pm', 'READ'),
(4, 5, 15010086, 88888888, '', '12 Dec 2016 - 12:43 pm', 'READ'),
(5, 5, 15010086, 99999999, '', '12 Dec 2016 - 12:43 pm', 'UNREAD'),
(6, 1, 88888888, 10101010, '<p>Test</p>', '22 Dec 2016 - 03:52 am', 'READ'),
(7, 2, 10101010, 88888888, '<p>Test</p>', '22 Dec 2016 - 04:05 am', 'READ'),
(8, 2, 88888888, 10101010, '<p>asdasdasd</p>', '22 Dec 2016 - 04:07 am', 'READ'),
(9, 3, 10101010, 88888888, '', '22 Dec 2016 - 04:09 am', 'READ'),
(10, 1, 10101010, 88888888, '<p>Pesan</p>', '23 Dec 2016 - 03:04 am', 'READ'),
(11, 1, 88888888, 10101010, '<p>sdsd</p>', '23 Dec 2016 - 03:05 am', 'READ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_notification_general`
--

CREATE TABLE `project_notification_general` (
  `id_notification_general` int(11) NOT NULL,
  `sender_notification_general` int(11) NOT NULL,
  `receiver_notification_general` int(11) NOT NULL,
  `contents_notification_general` text NOT NULL,
  `date_notification_general` varchar(20) NOT NULL,
  `status_notification_general` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `project_submission`
--

CREATE TABLE `project_submission` (
  `id_submission` int(5) NOT NULL,
  `id_project_submission` int(11) NOT NULL,
  `contents_submission` text NOT NULL,
  `sender_submission` int(11) NOT NULL,
  `receiver_submission` int(11) NOT NULL,
  `answer_submission` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `project_submission`
--

INSERT INTO `project_submission` (`id_submission`, `id_project_submission`, `contents_submission`, `sender_submission`, `receiver_submission`, `answer_submission`) VALUES
(3, 28, 'Tidak ada', 10101010, 2333, 'Di tolak'),
(5, 28, 'asd', 10101010, 88888888, 'Di setujui'),
(6, 29, 'asdasdasd', 10101010, 2333, 'Menunggu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `public_account`
--

CREATE TABLE `public_account` (
  `id_user` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `id_department` int(3) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `public_account`
--

INSERT INTO `public_account` (`id_user`, `name`, `gender`, `id_department`, `username`, `password`, `level`) VALUES
(2333, 'Bill Gates', 'Pria', 1, 'bill', '123', 'Dosen'),
(11111, 'myAdmin', 'Pria', 0, 'admin', '123', 'Admin'),
(10101010, 'Digital Report', 'Wanita', 1, 'digital', '123', 'Mahasiswa'),
(15010075, 'Moch Tauladan', 'Pria', 1, 'dhany', '123', 'Mahasiswa'),
(15010077, 'Lily Anderson', 'Wanita', 1, 'lily', '123', 'Mahasiswa'),
(15010086, 'Muhammad Rizqi Alfaruq', 'Pria', 1, 'rizqialfaruq', '123', 'Mahasiswa'),
(15092023, 'Laila', 'Wanita', 3, 'laila', '123', 'Mahasiswa'),
(88888888, 'Ellya Nurfarida', 'Wanita', 1, 'ellya', '123', 'Dosen'),
(98945989, 'Andika Kurnia', 'Pria', 1, 'andika', '123', 'Dosen'),
(99999999, 'Kunti Elliyen', 'Wanita', 1, 'kunti', '123', 'Dosen'),
(123456789, 'Putut', 'Pria', 3, 'putut', '123', 'Dosen');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_category`
--
ALTER TABLE `project_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `project_file`
--
ALTER TABLE `project_file`
  ADD PRIMARY KEY (`id_file`);

--
-- Indexes for table `project_folder`
--
ALTER TABLE `project_folder`
  ADD PRIMARY KEY (`id_folder`);

--
-- Indexes for table `project_group`
--
ALTER TABLE `project_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_guide`
--
ALTER TABLE `project_guide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_master`
--
ALTER TABLE `project_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_notification_file`
--
ALTER TABLE `project_notification_file`
  ADD PRIMARY KEY (`id_notification_file`);

--
-- Indexes for table `project_notification_general`
--
ALTER TABLE `project_notification_general`
  ADD PRIMARY KEY (`id_notification_general`);

--
-- Indexes for table `project_submission`
--
ALTER TABLE `project_submission`
  ADD PRIMARY KEY (`id_submission`);

--
-- Indexes for table `public_account`
--
ALTER TABLE `public_account`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `project_category`
--
ALTER TABLE `project_category`
  MODIFY `id_category` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `project_file`
--
ALTER TABLE `project_file`
  MODIFY `id_file` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_folder`
--
ALTER TABLE `project_folder`
  MODIFY `id_folder` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_group`
--
ALTER TABLE `project_group`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `project_guide`
--
ALTER TABLE `project_guide`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `project_master`
--
ALTER TABLE `project_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `project_notification_file`
--
ALTER TABLE `project_notification_file`
  MODIFY `id_notification_file` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `project_notification_general`
--
ALTER TABLE `project_notification_general`
  MODIFY `id_notification_general` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_submission`
--
ALTER TABLE `project_submission`
  MODIFY `id_submission` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
