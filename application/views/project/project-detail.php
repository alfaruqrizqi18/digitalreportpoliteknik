<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Default Example -->
                <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                      <?php foreach ($show_project_name_by_id as $data_project_name_by_id) {?>
                        <div class="header bg-<?php echo $data_project_name_by_id['color']?>">
                            <h2>
                                <?php echo "Nama Proyek : ".$data_project_name_by_id['project_title']; ?>
                            </h2>
                            <?php } ?>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                      <?php
                                      $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
                                      if ($data_project_name_by_id['status'] == "PRIVATE") {
                                      if ($data_project_name_by_id['project_id_owner'] == $id_user ) { ?>
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                      <ul class="dropdown-menu pull-right">
                                      <li><a data-toggle="modal" data-target="#modal-add-folder">Buat Folder Baru </a></li>
                                      <li><a data-toggle="modal" data-target="#modal-add-file">Buat File Baru </a></li>
                                      </ul>
                                      <?php } else {
                                        foreach ($show_my_group_only_for_detail_option as $show_my_group_only_for_detail_option) {
                                          if ($show_my_group_only_for_detail_option['id_account'] == $id_user) {
                                        ?>
                                          <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                              <i class="material-icons">more_vert</i>
                                          </a>
                                      <ul class="dropdown-menu pull-right">
                                      <li><a data-toggle="modal" data-target="#modal-add-file">Buat File Baru </a></li>
                                      </ul>
                                      <?php }?>
                                      <?php }?>
                                      <?php }?>
                                      <?php }?>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="clearfix m-b-20">
                                <div class="dd">
                                    <ol class="dd-list">
                                      <?php foreach ($show_folder as $data_folder) {?>
                                        <li class="dd-item" data-id="<?php echo $data_folder['id_folder'] ?>">
                                            <div class="dd-handle"><?php echo $data_folder['name_folder']; ?></div>
                                            <ol class="dd-list">
                                              <?php foreach ($show_file as $data_file) {
                                                if ($data_folder['id_folder'] == $data_file['parent']) {?>
                                                  <a  class="show-modal" data-toggle="modal" data-target="#modal-manipulation-file"
                                                      data-id="<?php echo $data_file['id_file'] ?>"
                                                      data-title="<?php echo $data_file['name_file'] ?>"
                                                      data-content ='<?php echo $data_file['contents'] ?>'
                                                      data-creator = '<?php echo $data_file['created_by'] ?>'>
                                                <li class="dd-item">
                                                    <div class="dd-handle"><?php echo $data_file['name_file']; ?>
                                                      </div>
                                                </li>
                                                    </a>
                                                <?php }} ?>
                                            </ol>
                                        </li>
                                        <?php } ?>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

              <!-- Default Size -->
              <div class="modal fade" id="modal-add-folder" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form action="<?php echo base_url('project/add_file_folder'); ?>" method="post">
                          <div class="modal-header">
                              <h4 class="modal-title">Folder Baru</h4>
                          </div>
                          <div class="modal-body ">
                            <div class="body">
                                          <input readonly type="hidden" class="form-control" name="idproject" value="<?php echo $idprojectparsing; ?>"/>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="folder" placeholder="Nama Folder..." required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <input type="hidden" class="form-control" name="tipe" placeholder="" value="Folder" required/>
                                    </div>
                                </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-link waves-effect">BUAT</button>
                              <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                          </div>
                    </form>
                      </div>
                  </div>
              </div>

              <div class="modal fade" id="modal-add-file" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form action="<?php echo base_url('project/add_file_folder'); ?>" method="post">
                          <div class="modal-header">
                              <h4 class="modal-title">File Baru</h4>
                          </div>
                          <div class="modal-body ">
                            <div class="body">
                                      <input readonly type="hidden" class="form-control" name="idproject" value="<?php echo $idprojectparsing; ?>"/>

                                      <div class="form-group">
                                        <div class="row clearfix">
                                          <div class="col-sm-12">
                                            <select name="dirfolder" class="form-control show-tick" required="">
                                              <option value="">Tentukan di Folder mana Anda menyimpan...</option>
                                              <?php foreach ($show_folder as $data_folder) {?>
                                                <option value="<?php echo $data_folder['id_folder'] ?>"><?php echo $data_folder['name_folder']; ?></option>
                                                <?php } ?>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                          <div class="form-line">
                                              <input type="text" class="form-control" name="file" placeholder="Nama File..." required/>
                                          </div>
                                      </div>
                                      <div class="form-group">
                                              <input type="hidden" class="form-control" name="tipe" placeholder="" value="File" required/>
                                      </div>
                                </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-link waves-effect">BUAT</button>
                              <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                          </div>
                    </form>
                      </div>
                  </div>
              </div>


              <div class="modal fade" id="modal-manipulation-file" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                              <h4 class="modal-title" id="title">
                              </h4>
                          </div>
                          <div class="modal-body ">
                            <div class="body">
                              <div class="form-group">
                                  <input readonly type="hidden" class="form-control" name="file" id="idfile" required/>
                              </div>
                              <data id="content"></data>
                                </div>
                          </div>
                        <div class="modal-footer">
                          <?php foreach ($show_project_name_by_id as $data) {
                            if ($data['status'] == 'PRIVATE' && $this->session->userdata('DIGITAL_REPORT_LEVEL') != "") {
                            ?>
                              <a id="file-editor"><button type="submit" class="btn btn-success waves-effect">REVISI</button></a>
                          <?php } ?>
                          <?php } ?>
                          <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                        </div>
                      </div>
                  </div>
              </div>


            </div>
          </section>
