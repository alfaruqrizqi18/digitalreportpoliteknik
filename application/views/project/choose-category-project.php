    <section class="content">
        <div class="container-fluid">
          <div class="row clearfix">
            <?php foreach ($show_all_project_category as $data) { ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header <?php echo "bg-".$data['color']; ?>">
                            <h2>
                                <?php echo $data['name_category']; ?>
                            </h2>
                        </div>
                        <div class="body">
                          <p>
                            <?php echo $data['desc_category']; ?>
                          </p>
                          <div class="button-demo js-modal-buttons">
                            <h1>
                              <a onClick="$('#modal-dialog-thesis').modal('show')"
                                data-id="<?php echo $data['id_category'] ?>"
                                data-title="<?php echo $data['name_category'] ?>"
                                <button type="button"
                                class="btn btn-block show-modal waves-effect <?php echo "bg-".$data['color']; ?>"
                                data-toggle="modal" data-target="#defaultModal">
                                <?php echo $data['text_button_category']; ?></button>
                              </a>
                            </h1>
                          </div>
                        </div>
                    </div>
                </div>
              <?php }?>
            </div>

            <!-- MODAL DIALOG THESIS -->
            <div class="modal fade" id="modal-dialog-thesis" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <form action="<?php echo base_url('project/add_project'); ?>" method="post">
                        <div class="modal-header">
                            <h4 class="modal-title">Form Pengisian Judul Laporan</h4>
                        </div>
                        <div class="modal-body ">
                          <div class="body">
                                  <input type="hidden" class="form-control" name="id_category" id="id_category"/>
                                    <p><h5>Judul Laporan</h5></p>
                                  <div class="form-group">
                                      <div class="form-line">
                                          <input type="text" class="form-control" name="title" placeholder="Masukkan Judul Laporanmu...." required/>
                                      </div>
                                  </div>
                              </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-link waves-effect">BUAT</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TUTUP</button>
                        </div>
                  </form>
                    </div>
                </div>
            </div>
            <!-- MODAL DIALOG THESIS -->
          </div>
    </section>
