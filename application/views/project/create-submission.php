<section class="content">
        <div class="container-fluid">
          <div class="row clearfix">
              <!-- Default Example -->
              <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header bg-blue">
                        <h2>Form Pengajuan Judul</h2>
                      </div>
                      <div class="body">
                        <form id="form_validation" action="<?php echo base_url('project/add_submission') ?>" method="POST">
                            <div class="form-group">
                              <div class="row clearfix">
                                <div class="col-sm-12">
                                  <p><h5>Pilih Laporan yang ingin diajukan</h5></p>
                                  <select name="project" class="form-control show-tick" required="" data-live-search="true">
                                      <option value="">Pilih Judul Laporanmu</option>
                                      <?php foreach ($show_all_my_project_for_submission as $data_my_project) {?>
                                      <option value="<?php echo $data_my_project['id_project']?>">
                                      <?php echo $data_my_project['project_title']; ?></option>
                                      <?php } ?>
                                  </select>
                                </div>
                                  <div class="col-sm-12">
                                    <p><h5>Pilih Dosen Pembimbing</h5></p>
                                    <select name="guide" class="form-control show-tick" required="" data-live-search="true">
                                        <option value="">Belum memilih dosen</option>
                                        <?php foreach ($show_account_dosen as $data_dosen) {?>
                                        <option value="<?php echo $data_dosen['id_user']; ?>"><?php echo $data_dosen['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                  </div>
                              </div>
                            </div>
                            <div class="form-group form-float">
                              <h3 class="card-inside-title">Penjelasan</h3>
                              <div class="col-sm-12">
                                      <div class="form-group">
                                          <div class="form-line">
                                              <textarea rows="4" name="contents" class="form-control no-resize" placeholder="Jelaskan secara singkat disini...." required=""></textarea>
                                          </div>
                                      </div>
                              </div>
                            </div>
                            <button class="btn btn-primary waves-effect" type="submit">Kirim Pengajuan</button>
                        </form>
                      </div>
                  </div>
              </div>
            </div>
        </div>
</section>
