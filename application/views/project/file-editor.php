<!DOCTYPE html>
<base href="<?php echo base_url() ?>" target="">
<html lang="en">
<head>
  <meta charset="UTF-8">
  <?php foreach ($show_file_to_update as $data) {?>
  <title><?php echo $data['name_file']; ?></title>
  <?php } ?>
  <link href="assets-summernote/css/bootstrap.css" rel="stylesheet">
  <script src="assets-summernote/js/jquery.js"></script>
  <script src="assets-summernote/js/bootstrap.js"></script>
  <link href="assets-summernote/css/summernote.css" rel="stylesheet">
  <script src="assets-summernote/js/summernote.js"></script>

  <script>
    $(document).ready(function() {
      $('#summernote').summernote({
        toolbar: [
        // [groupName, [list of button]]
        ['misc', ['undo', 'redo', 'help','fullscreen']],
        ['style', ['bold', 'para', 'italic', 'underline', 'clear']],
        ['color', ['color']],
        ['fontsize', ['fontsize','fontname']],
        ['insert', ['picture', 'link', 'table', 'hr']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']]
      ],
        height: 280,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true                  // set focus to editable area after initializing summernote
        });
      $('#summernote-for-commit').summernote({
        toolbar: [
        // [groupName, [list of button]]
        ['misc', ['undo', 'redo', 'help']],
        ['style', ['bold', 'para', 'italic', 'underline']],
        ['color', ['color']],
        ['fontsize', ['fontsize','fontname']],
        ['para', ['ul', 'ol', 'paragraph']]
      ],
        placeholder: 'Tulis apa yang baru saja diubah......',
        height: 100,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null                // set focus to editable area after initializing summernote
        });

    });
  </script>

</head>
<body>
  <div class="panel panel-default">
    <?php foreach ($show_file_to_update as $data) {?>
      <!-- ---------------------------------------------->
    <div class="panel-heading">
    <a href="javascript:history.back()">
      <button type="button" class="btn btn-success btn-sm ">Kembali </button>
    </a>
      Author : <b><?php echo $data['name']; ?></b> <b> ~ </b> Created on : <b><?php echo $data['date_create']; ?></b>
      <b> ~ </b>Last Modifier : <b><?php echo $data['last_modifier']; ?></b> <b> ~ </b> Last Modified : <b><?php echo $data['last_modified_date']; ?></b>
      <?php if ($data['created_by'] == $this->session->userdata('DIGITAL_REPORT_ID_USERS')) {?>
      <a href="<?php echo base_url('project/delete_file/'.$data['id_file']."/".base64_encode($data['id_project'])) ?>"><button type="button" class="btn btn-danger btn-sm">Hapus file ?</button></a>
        <?php } ?>
    </div>
    <?php } ?>
    <!-- ---------------------------------------------->

    <!-- ---------------------------------------------->
    <div class="panel-body">
      <?php foreach ($show_file_to_update as $data) {?>
        <form action="<?php echo base_url('project/update_file_editor_and_insert_notification/'.$data['id_file']) ?>" method="post">
          <textarea name="contents" id="summernote"><?php echo $data['contents']; ?></textarea>
          <textarea name="message" id="summernote-for-commit"></textarea>
          <input type="hidden" name="created_by" value="<?php echo $data['created_by'] ?>">
          <input type="hidden" name="last_modifier" value="<?php echo $data['last_modified_by']; ?>">
          <!-- UNTUK VALIDASI BUTTON SIMPAN AGAR MENYALA SAAT SESSION NIM DENGAN PEMBUAT FILE SAMA -->
            <?php if ($data['created_by'] == $this->session->userdata('DIGITAL_REPORT_ID_USERS') ) { ?>
            <button type="submit" class="btn btn-primary ">Simpan</button>
              <?php } ?>
          <?php }?>
          <!-- UNTUK VALIDASI BUTTON SIMPAN AGAR MENYALA SAAT SESSION NIM DENGAN PEMBUAT FILE SAMA -->

          <!-- UNTUK VALIDASI BUTTON SIMPAN AGAR MENYALA SAAT SESSION NIM DENGAN DOSEN PEMBIMBING DALAM SATU PROYEK YANG SAMA -->
          <?php foreach ($show_button_save_update as $data_btn) {?>
            <?php if ($data_btn['id_account'] == $this->session->userdata('DIGITAL_REPORT_ID_USERS') ) { ?>
            <button type="submit" class="btn btn-primary ">Simpan</button>
              <?php } ?>
          <?php } ?>
        <!-- UNTUK VALIDASI BUTTON SIMPAN AGAR MENYALA SAAT SESSION NIM DENGAN DOSEN PEMBIMBING DALAM SATU PROYEK YANG SAMA -->

        </form>
    </div>
    <!-- ---------------------------------------------->


    <!-- ---------------------------------------------->
    <div class="panel-footer">
      File Editor : <b>Summernote Editor v0.8.2</b> - <b>Digital Report</b>
    </div>
    <!-- ---------------------------------------------->
  </div>
</body>
</html>
