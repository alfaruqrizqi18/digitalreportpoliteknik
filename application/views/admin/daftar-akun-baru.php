<section class="content">
        <div class="container-fluid">
            <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>Form Pendaftaran Akun Baru</h2>
                        </div>
                        <div class="body">
                            <form id="form_validation" action="<?php echo base_url('admin/daftar_akun') ?>" method="POST">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="id_user" required numberonly>
                                        <label class="form-label">NIM / NIDN</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="nama" required>
                                        <label class="form-label">Nama</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="row clearfix">
                                    <div class="col-sm-12">
                                      <select name="gender" class="form-control show-tick" required="">
                                          <option value="">Pilih Jenis Kelamin</option>
                                          <option value="Pria">Pria</option>
                                          <option value="Wanita">Wanita</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="row clearfix">
                                    <div class="col-sm-12">
                                      <select name="jurusan" class="form-control show-tick" required="">
                                          <option value="">Pilih Jurusan</option>
                                          <option value="0">Admin - Hanya untuk Admin</option>
                                          <?php foreach ($tampil_data_department as $data_department) {?>
                                          <option value="<?php echo $data_department['id']; ?>"><?php echo $data_department['name_department']; ?></option>
                                          <?php } ?>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="username" required>
                                        <label class="form-label">Username</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="password" required>
                                        <label class="form-label">Password</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="row clearfix">
                                    <div class="col-sm-12">
                                      <select name="level" class="form-control show-tick" required="">
                                          <option value="">Pilih Level</option>
                                          <option value="Mahasiswa">Mahasiswa</option>
                                          <option value="Dosen">Dosen</option>
                                          <option value="Admin">Admin</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Validation -->
          </div>
        </section>
