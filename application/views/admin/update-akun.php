<section class="content">
        <div class="container-fluid">
            <!-- Basic Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>Form Update Akun</h2>
                        </div>
                        <div class="body">
                          <?php foreach ($data_akun_per_id as $data) {?>
                            <form  action="<?php echo base_url('admin/update_akun/'.$data['id_user']) ?>" method="POST">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="nama" required value="<?php echo $data['name'] ?>" placeholder="Nama">
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="row clearfix">
                                    <div class="col-sm-12">
                                      <select name="gender" class="form-control show-tick" required="">
                                          <option value="<?php echo $data['gender']; ?>">Jenis Kelamin saat ini : <?php echo $data['gender']; ?></option>
                                          <option value="">Pilih Jenis Kelamin</option>
                                          <option value="Pria">Pria</option>
                                          <option value="Wanita">Wanita</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="row clearfix">
                                    <div class="col-sm-12">
                                      <select name="jurusan" class="form-control show-tick" required="">
                                          <option value="<?php echo $data['id_department']; ?>">Jurusan saat ini : <?php echo $data['name_department']; ?></option>
                                          <option value="">Pilih Jurusan</option>
                                          <option value="0">Admin - Hanya untuk Admin</option>
                                          <?php foreach ($tampil_data_department as $data_department) {?>
                                          <option value="<?php echo $data_department['id']; ?>"><?php echo $data_department['name_department']; ?></option>
                                          <?php } ?>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="username" required value="<?php echo $data['username']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" class="form-control" name="password" required value="<?php echo $data['password']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                  <div class="row clearfix">
                                    <div class="col-sm-12">
                                      <input type="hidden" name="current-level" value="<?php echo $data['level']; ?>">
                                      <select name="level" class="form-control show-tick" required="">
                                          <option value="<?php echo $data['level']; ?>">Level saat ini : <?php echo $data['level']; ?></option>
                                          <option value="">Pilih Level</option>
                                          <option value="Mahasiswa">Mahasiswa</option>
                                          <option value="Dosen">Dosen</option>
                                          <option value="Admin">Admin</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">Update</button>
                            </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Validation -->
          </div>
        </section>
