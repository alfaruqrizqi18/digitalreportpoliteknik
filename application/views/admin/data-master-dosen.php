<section class="content">
    <div class="container-fluid">
  <!-- Hover Rows -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-deep-orange">
                            <h2>
                                Data Master Akun Dosen

                            </h2>
                            <ul class="header-dropdown m-r--5">
                            <li>
                                <a href="<?php echo base_url('admin/daftar') ?>" role="button" class="show-modal" >
                                      <i class="material-icons">person_add</i>
                                </a>
                              </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>NIM</th>
                                        <th>NAMA</th>
                                        <th>GENDER</th>
                                        <th>JURUSAN</th>
                                        <th>USERNAME</th>
                                        <th>LEVEL</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($data_dosen as $data) {
                                    if ($data['id_department'] == 0) {
                                      $name_department = "Admin";
                                    }else{
                                      $name_department = $data['name_department'];
                                    }
                                    ?>
                                    <tr>
                                        <th scope="row"><?php echo $data['id_user']; ?></th>
                                        <td><?php echo $data['name']; ?></td>
                                        <td><?php echo $data['gender']; ?></td>
                                        <td><?php echo $name_department; ?></td>
                                        <td><?php echo $data['username']; ?></td>
                                        <td><span class="badge bg-deep-orange"><?php echo $data['level']; ?></span></td>
                                        <td>
                                          <div class="btn-group">
                                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                              ACTION <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu">
                                              <li><a href="<?php echo base_url('admin/form_update/'.$data['id_user']) ?>" class=" waves-effect waves-block">Update</a></li>
                                              <li><a href="<?php echo base_url('admin/hapus_akun/'.$data['id_user']) ?>" class=" waves-effect waves-block">Hapus</a></li>
                                          </ul>
                                        </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Hover Rows -->

      </div>
</section>
