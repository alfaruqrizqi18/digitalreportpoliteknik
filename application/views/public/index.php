    <section class="content">
        <div class="container-fluid">
            <!-- Basic Example -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Digital Report<small>Administrator</small>
                            </h2>
                        </div>
                        <div class="body">
                            <h1 class="align-center font-45">Selamat Datang di Digital Report</h1>
                            <p class="font-17 font-normal align-center">
                              <b>Digital Report</b> adalah sebuah aplikasi web yang digunakan untuk membuat berbagai macam laporan untuk membantu tugas mahasiswa.
                              Dengan menggunakan <b>Digital Report</b>, kamu dapat menyimpan berbagai macam laporan yang telah kamu buat untuk disimpan ke <b>penyimpanan-digital</b> kami.
                              Jika kamu ingin mencari referensi tentang laporan, kamu dapat mencarinya melalui <b>form-fill</b> dibawah ini.
                            </p>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                  <form action="<?php echo base_url('search/result') ?>" method="get">
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="text" name="search-field" class="form-control align-center font-20 date" placeholder="Cari referensi laporan disini..." autofocus required>
                                        </div>
                                          <h1 class="align-center">
                                          <button type="submit" class="btn btn-lg btn-primary waves-effect"><h4>Cari sekarang!</h4></button>
                                        </h1>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
          </div>
        </div>
    </section>
