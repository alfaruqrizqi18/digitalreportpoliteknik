<style>
  .garis{
    border-color: lightgray;
  }
</style>
<section class="content">
    <div class="container-fluid">
        <!-- Basic Example -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header bg-blue">
                        <h2>
                            Tentang Digital Report
                        </h2>
                    </div>
                    <div class="body">
                      <div class="card">
                        <div class="header bg-blue">
                        </div>
                        <div class="body">
                            <p class="lead">
                                <b>Apa itu Digital Report ?</b>
                            </p>
                            <p class="font-15 align-justify">
                              Digital Report adalah sebuah aplikasi berbasis web yang dapat membantu para pelajar
                              khususnya Mahasiswa dan Guru atau Dosen dalam rangka pembuatan laporan dan bimbingan laporan.
                              Laporan seperti Skripsi, Thesis dll yang membutuhkan pendamping atau pembimbing dapat menggunakan
                              Digital Report sebagai salah satu solusinya.
                            </p>
                            <hr class="garis">
                            <p class="lead">
                                <b>Latar Belakang Terciptanya Digital Report</b>
                            </p>
                            <p class="font-15">
                              Latar Belakang
                            </p>
                            <hr class="garis">
                            <p class="lead">
                                <b>Tim Digital Report</b>
                            </p>
                            <div class="row align-center">
                              <div class="col-sm-6 col-md-6">
                                    <div class="thumbnail">
                                        <img src="assets/images/dewa.jpg">
                                        <div class="caption">
                                            <h3>Muhammad Rizqi Alfaruq</h3>
                                            <p>
                                                <a href="www.facebook.com/rizqialfaruq24" class="btn btn-primary waves-effect" role="button">BUTTON</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                  <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="assets/images/dewa.jpg">
                                            <div class="caption">
                                                <h3>Sapna Dewi Arlingga Sari</h3>
                                                <p>
                                                    <a href="javascript:void(0);" class="btn btn-primary waves-effect" role="button">BUTTON</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <!--<div class="card">
                      <div class="header bg-pink">
                      </div>
                      <div class="body">
                          <p class="lead">
                              <b>Digital Report Meme's</b>
                          </p>
                          <p class="font-15">
                            Pernahkah kamu mengalami ini ?
                          </p>
                      </div>
                    </div> -->

                    </div>
                </div>
          </div>
      </div>
    </div>
</section>
