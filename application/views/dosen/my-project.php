
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Example -->
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header bg-blue">
                          <h2>
                              Digital Report<small>Laporan-ku</small>
                          </h2>
                          <ul class="header-dropdown m-r--5">
                          </ul>
                      </div>
                        <div class="body">
                          <?php foreach ($show_all_my_project as $data) {?>
                          <div class="card">
                              <div class="header <?php echo "bg-".$data['color'] ?>">
                                  <h2>
                                      Tipe : <?php echo $data['name_category']; ?>
                                  </h2>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <div class="demo-google-material-icon">
                                      <?php if ($data['project_id_owner'] == $this->session->userdata['DIGITAL_REPORT_ID_USERS']) { ?>
                                      <a  role="button" class="show-modal" onClick=$("#modal-add-group").modal('show') data-id= <?php echo $data['id_project']; ?> >
                                          <i class="material-icons">person_add</i>
                                      </a>
                                      <?php } ?>
                                    </div>
                                </li>

                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                      <ul class="dropdown-menu pull-right">
                                          <?php if ($data['project_id_owner'] == $this->session->userdata['DIGITAL_REPORT_ID_USERS']) {?>
                                          <li><a href="<?php echo base_url('project/delete_project/'.$data['id_project']) ?>">Hapus Project</a></li>
                                          <li><a href="<?php echo base_url('project/update_project_to_public/'.$data['id_project']) ?>">Rilis Project</a></li>
                                          <li class="divider"></li>
                                          <?php } else { ?>
                                            <li></li>
                                            <?php } ?>
                                          <li><a href="<?php echo base_url('project/detail/'.base64_encode($data['id_project'])); ?>" target="_blank" >Open in new tab</a></li>
                                      </ul>
                                </li>
                            </ul>
                              </div>
                              <div class="body">
                                <p class="lead">
                                <a href="<?php echo base_url('project/detail/'.base64_encode($data['id_project'])); ?>"><?php echo $data['project_title']." "; ?></a>
                                </p>
                                <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active">
                                      <a href="#informasi_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">info</i> Informasi Tentang Laporan
                                      </a>
                                  </li>
                                  <li role="presentation">
                                      <a href="#pembimbing_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">accessibility</i> Pembimbing Laporan
                                      </a>
                                  </li>
                                  <li role="presentation">
                                      <a href="#ketua_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">stars</i> Ketua Kelompok
                                      </a>
                                  </li>
                                  <li role="presentation_<?php echo $data['id_project'] ?>">
                                      <a href="#anggota_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">group</i> Anggota Kelompok
                                      </a>
                                  </li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <!-- INFORMASI LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in active" id="informasi_<?php echo $data['id_project'] ?>">
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5>Status : <?php echo $data['status']; ?></h5></span>
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5>Dibuat : <?php echo $data['project_date_create']; ?></h5></span>
                                  </div>
                                    <!-- INFORMASI LAPORAN -->

                                  <!-- PEMBIMBING LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in " id="pembimbing_<?php echo $data['id_project'] ?>">
                                    <?php foreach ($show_my_guide as $data_guide) {?>
                                    <?php if ($data_guide['id'] == $data['id_project'] ) {?>
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5><?php echo $data_guide['guide']; ?></h5></span>
                                    <?php } ?>
                                    <?php } ?>
                                  </div>
                                 <!-- PEMBIMBING LAPORAN -->

                                  <!-- KETUA LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in " id="ketua_<?php echo $data['id_project'] ?>">
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5><?php echo $data['name']; ?></h5></span>
                                  </div>
                                  <!-- KETUA LAPORAN -->

                                  <!-- ANGGOTA LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in " id="anggota_<?php echo $data['id_project'] ?>">
                                  <?php foreach ($show_my_group as $group) {
                                    if ($group['id'] != $data['id_project']) { ?>
                                    <?php } else {
                                      if ($data['project_id_owner'] == $this->session->userdata['DIGITAL_REPORT_ID_USERS']) {?>
                                      <span class="badge bg-white">
                                      <a href="<?php echo base_url('project/delete_group/'.$group['id_group']) ?>">
                                        <button type="button" class="btn btn-danger btn-xs waves-effect">
                                          <i class="material-icons">delete_forever</i>
                                        </button>
                                      </a>
                                    </span>
                                    <?php } ?>
                                    <span class="badge <?php echo "bg-".$data['color'] ?>">
                                      <h5><?php echo $group['nama_kelompok']; ?></h5>
                                    </span>
                                    <?php }} ?>
                                  </div>
                                  <!-- KETUA LAPORAN -->

                              </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                  </div>
              </div>
              <!--row clearfix -->
          </div>
        </div>
    </section>
