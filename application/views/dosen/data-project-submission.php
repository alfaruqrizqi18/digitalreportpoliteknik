<section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Data Pengajuan Judul Laporan
                            </h2>
                        </div>
                        <div class="body">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active">
                                  <a href="#pending" data-toggle="tab">
                                      <i class="material-icons">assignment_late</i> PENDING
                                  </a>
                              </li>
                              <li role="presentation">
                                  <a href="#terjawab" data-toggle="tab">
                                      <i class="material-icons">settings</i> TERJAWAB
                                  </a>
                              </li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane fade in active" id="pending">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                          <th>Judul Laporan</th>
                                          <th>Deskripsi</th>
                                          <th>Mahasiswa</th>
                                          <th>Dosen</th>
                                          <th>Jawaban</th>
                                          <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Judul Laporan</th>
                                          <th>Deskripsi</th>
                                          <th>Mahasiswa</th>
                                          <th>Dosen</th>
                                          <th>Jawaban</th>
                                          <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($show_my_submission as $data) {?>
                                        <tr>
                                            <td><?php echo $data['project_title']; ?></td>
                                            <td><?php echo $data['contents_submission']; ?></td>
                                            <?php foreach ($show_sender_submission as $data_sender) {?>
                                              <?php if ($data['id_submission'] == $data_sender['id_submission']) {?>
                                                  <td><?php echo $data_sender['sender_submission']; ?></td>
                                              <?php } ?>
                                            <?php } ?>
                                            <td><?php echo $data['receiver_submission']; ?></td>
                                            <td><?php echo $data['answer_submission']; ?></td>
                                            <td align="center">
                                              <?php if ($data['answer_submission'] == "Menunggu"){?>
                                                <a href="<?php echo base_url('project/add_guide/'.$data['id_project_submission']."/".$data['id_submission']) ?>"><button type="button" class="btn btn-primary  waves-effect">
                                                  <i class="material-icons">done_all</i>
                                                </button>
                                                </a>
                                                <a href="<?php echo base_url('project/decline_submission/'.$data['id_submission']) ?>"><button type="button" class="btn bg-pink waves-effect">
                                                  <i class="material-icons">pan_tool</i>
                                                </button>
                                                </a>
                                              <?php } ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="terjawab">
                                  <table class="table table-bordered table-striped table-hover js-basic-example ">
                                      <thead>
                                          <tr>
                                            <th>Judul Laporan</th>
                                            <th>Deskripsi</th>
                                            <th>Mahasiswa</th>
                                            <th>Dosen</th>
                                            <th>Jawaban</th>
                                            <th>Action</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                            <th>Judul Laporan</th>
                                            <th>Deskripsi</th>
                                            <th>Mahasiswa</th>
                                            <th>Dosen</th>
                                            <th>Jawaban</th>
                                            <th>Action</th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php foreach ($show_my_submission_answered as $data) {?>
                                          <tr>
                                              <td><?php echo $data['project_title']; ?></td>
                                              <td><?php echo $data['contents_submission']; ?></td>
                                              <?php foreach ($show_sender_submission as $data_sender) {?>
                                                <?php if ($data['id_submission'] == $data_sender['id_submission']) {?>
                                                    <td><?php echo $data_sender['sender_submission']; ?></td>
                                                <?php } ?>
                                              <?php } ?>
                                              <td><?php echo $data['receiver_submission']; ?></td>
                                              <?php
                                              if ($data['answer_submission'] == "Di setujui") {
                                                $background = "badge bg-green";
                                              }else if ($data['answer_submission'] == "Di tolak") {
                                                $background = "badge bg-pink";
                                              }
                                               ?>
                                              <td><span class="<?php echo $background; ?>"><?php echo $data['answer_submission']; ?></span></td>
                                              <td align="center">
                                                <?php if ($data['status'] == "PUBLIC" && $data['answer_submission'] == "Di setujui"){?>
                                                  <span class="badge bg-green">Laporan ini telah dirilis</span>
                                                  <?php } else if ($data['answer_submission'] == "Di setujui"){ ?>
                                                    <a href="<?php echo base_url('project/delete_guide/'.$data['id_project_submission']."/".$data['id_submission']) ?>"><button type="button" class="btn btn-danger waves-effect">
                                                      Cancel
                                                    </button>
                                                    </a>
                                                    <?php } ?>
                                              </td>
                                          </tr>
                                          <?php } ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
      </div>
</section>
