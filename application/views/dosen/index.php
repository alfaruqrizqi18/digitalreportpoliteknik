
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Example -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Digital Report<small>Administrator</small>
                            </h2>
                        </div>
                        <div class="body">
                          <p class="font-50 font-bold align-center">Selamat Datang di Digital Report</p>
                            <p class="font-17 font-normal align-center">
                              <b>Digital Report</b> adalah sebuah aplikasi web yang digunakan untuk membuat berbagai macam laporan untuk membantu tugas Mahasiswa Politeknik Kediri.
                              Dan jika menggunakan <b>Digital Report</b>, kami dapat menyimpan berbagai macam laporan yang telah kamu buat untuk disimpan ke <b>penyimpanan-digital</b> kami.
                              Dengan menekan tombol <b>Baca Panduan</b> kamu akan membaca panduan tentang bagaimana menggunakan Digital Report. Dan kamu bisa langsung mencoba untuk membuat laporan dengan menekan tombol <b>Mulai Sekarang</b>
                            </p>
                              <div class="demo-button">
                                <h1 class="align-center">
                                <a href="<?php echo base_url('manualguide') ?>"><button type="button" class="btn btn-lg btn-primary waves-effect"><h4>Baca Panduan</h4></button></a>
                              </h1>
                              </div>
                            </div>

                        </div>
                    </div>
              </div>

              <!-- INFOBOX  -->
                <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box-4 hover-zoom-effect">
                              <div class="icon">
                                  <i class="material-icons col-pink">email</i>
                              </div>
                              <div class="content">
                                  <div class="text">PESAN</div>
                                  <div class="number">15</div>
                              </div>
                          </div>

                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box-4 hover-zoom-effect">
                              <div class="icon">
                                  <i class="material-icons col-blue">archive</i>
                              </div>
                              <div class="content">
                                  <div class="text">PROYEK-KU</div>
                                  <div class="number">92</div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box-4 hover-zoom-effect">
                              <div class="icon">
                                  <i class="material-icons col-blue">feedback</i>
                              </div>
                              <div class="content">
                                  <div class="text">UMPAN BALIK</div>
                                  <div class="number">1002</div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box-4 hover-zoom-effect">
                              <div class="icon">
                                  <i class="material-icons col-blue">favorite</i>
                              </div>
                              <div class="content">
                                  <div class="text">RESPON</div>
                                  <div class="number">92%</div>
                              </div>
                          </div>
                      </div>
              </div>
              <!-- INFOBOX  -->

          </div>
        </div>
    </section>
