<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Tentang Hak Privilege
                                <small>Panduan ini akan membantu pengguna yaitu Dosen, agar mengetahui privilege di Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#1. Are you a Report Guide ?</h3><hr>
                                        <p class="col-black align-justify">
                                            Apakah kamu seorang <b>Pembimbing Laporan ?</b>. <b>Tugas seorang Pembimbing Laporan sangatlah penting</b>.
                                            Karena kamu akan membantu para <b>Mahasiswa</b> dalam mengerjakan laporan-laporannya. Apakah kamu sudah mengerti <b>Privilege</b>
                                            apa saja jika kamu seorang <b>Pembimbing Laporan</b> ?
                                            <b>Privilege</b> di dalam Digital Report.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.1 Title Bar Icon</h3><hr>
                                                <p class="col-black align-justify">
                                                  Dalam <b>Title Bar</b> kamu <b>tidak akan disediakan 1 icon tambahan</b> yang berfungsi sebagai
                                                  penambahan anggota kelompok dalam laporan.
                                                </p>
                                            </div>
                                              <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/19.2.png" ?>">
                                              </div>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.2 Option Menu</h3><hr>
                                                <p class="col-black align-justify">
                                                  Dalam <b>Title Bar</b> icon <b>Opsi</b> hanya tersedia satu menu untuk <b>Pembimbing Laporan</b> yaitu,
                                                  <b>Buka di tab baru</b>.
                                                </p>
                                            </div>
                                            <div class="thumbnail">
                                                  <img src="<?php echo base_url()."assets/images/manual-guide/21.2.png" ?>">
                                            </div>
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                  <h5>#INFORMATION</h5>
                                                  <table class="table table-bordered table-hover table-striped" style="black">
                                                      <thead>
                                                          <tr>
                                                              <th>Menu</th>
                                                              <th>Penjelasan</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Buka di tab baru</th>
                                                              <td>Untuk membuka secara detail laporan di tab baru pada browser.</td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                                  </p>
                                              </div>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3 Tab Menu</h3><hr>
                                                <p class="col-black align-justify">
                                                  Informasi mengenai laporan yang kamu bimbing, telah disediakan dalam <b>4 tab menu</b> dan masing-masing
                                                  tab menu menyediakan informasi yang berbeda.
                                                </p>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.1 Tab Informasi Tentang Laporan</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Status Laporan</b> dan <b>Tanggal Pembuatan Laporan</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/22.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.2 Tab Pembimbing Tentang Laporan</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Nama dari Dosen Pembimbing Laporan</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/23.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.3 Tab Ketua Kelompok</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Nama dari Ketua Kelompok</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/24.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.4 Tab Anggota Kelompok</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Nama-nama dari seluruh Anggota Kelompok</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/26.png" ?>">
                                            </div>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.4 Button in Text Editor</h3><hr>
                                                <p class="col-black align-justify">
                                                  Dalam <b>Title Bar</b> di <b>Text Editor</b> tersedia tombol
                                                  <button type="button" class="btn btn-success waves-effect">
                                                    Kembali
                                                  </button> kemudian disisi kanan <b>Title Bar</b> terdapat tombol
                                                  <button type="button" class="btn btn-danger waves-effect">
                                                    Hapus File ?
                                                  </button> dan tombol terakhir berada disisi bawah text editor, yaitu tombol
                                                  <button type="button" class="btn btn-primary waves-effect">
                                                    Simpan
                                                  </button> seperti gambar dibawah ini.
                                                </p>
                                            </div>
                                            <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/29.1.png" ?>">
                                            </div>
                                            <div class="caption">
                                                <p class="col-black align-justify">
                                                  <h5>#PRIVILEGE-INFORMATION</h5>
                                                  <table class="table table-bordered table-hover table-striped" style="black">
                                                      <thead>
                                                          <tr>
                                                              <th>Login sebagai</th>
                                                              <th>
                                                                <button type="button" class="btn btn-success waves-effect">
                                                                  Kembali
                                                                </button>
                                                              </th>
                                                              <th>
                                                                <button type="button" class="btn btn-danger waves-effect">
                                                                  Hapus File ?
                                                                </button>
                                                              </th>
                                                              <th>
                                                                <button type="button" class="btn btn-primary waves-effect">
                                                                  Simpan
                                                                </button>
                                                              </th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Author File</th>
                                                              <td><span class="label bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Dosen Pembimbing</th>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-red">Tidak Tersedia</span></td>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Tanpa Login</th>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-red">Tidak Tersedia</span></td>
                                                              <td><span class="badge bg-red">Tidak Tersedia</span></td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                                </p>
                                            </div>
                                            <div class="caption">
                                              <p class="col-black align-justify">
                                                <b>Selamat untuk kamu,</b> karena telah membaca panduan ini. Sekarang kamu jadi lebih mengerti tentang <b>Privilege</b> di dalam Digital Report.
                                              </p>
                                            </div>
                                          </div>

                                        </div>
                                    </div>
                                </div>
                                <p class="align-center">
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-link waves-effect" role="button">Baca Panduan sebelumnya</a>
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya di Digital Report</a>
                                </p>
                            </div>
                        </div> <!--BODY -->
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
