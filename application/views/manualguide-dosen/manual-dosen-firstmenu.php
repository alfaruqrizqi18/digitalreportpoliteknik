<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Tentang Navigasi
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui fungsi-fungsi navigasi Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#1. Digital Report Navigation</h3><hr>
                                        <p class="col-black align-justify">
                                          Dalam panduan ini, kamu akan dijelaskan tentang fungsi-fungsi navigasi yang telah tersedia di Digital Report. Kamu bisa melihat gambar dibawah ini.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                            <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/31.png" ?>">
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                    <h5>#INFORMATION</h5>
                                                    <table class="table table-bordered table-hover table-striped" style="black" border="black">
                                                        <thead>
                                                            <tr>
                                                                <th>Nomor</th>
                                                                <th>Penjelasan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">#1</th>
                                                                <td>Nama aplikasi.</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">#2</th>
                                                                <td>Icon yang berfungsi sebagai notifikasi atau pemberitahuan antara Mahasiswa dengan Dosen Pembimbingnya terkait laporan yang telah dibuat.</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">#3</th>
                                                                <td>Icon yang memiliki fungsi sebagai pencarian laporan sebagai referensi sebelum membuat laporan.</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">#4</th>
                                                                <td>Nama dari pemilik akun yang telah login.</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">#5</th>
                                                                <td>USER-ID yang berfungsi sebagai nomer identitas setiap ID.</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">#6</th>
                                                                <td>Menu utama yang telah tersedia untuk berbagai kebutuhan pengguna.</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">#7</th>
                                                                <td>Menu yang berfungsi sebagai logout atau keluar dari aplikasi.</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                  </p>
                                              </div>
                                            </div>
                                            <p class="align-center">
                                            <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!--BODY -->
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
