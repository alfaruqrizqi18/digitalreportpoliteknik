<!-- Jquery Core Js -->
<script src="assets/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="assets/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="assets/plugins/node-waves/waves.js"></script>

<!-- Custom Js -->
<script src="assets/js/admin.js"></script>

<!-- Select Plugin Js -->
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="assets/js/pages/tables/jquery-datatable.js"></script>

<!-- Demo Js -->
<script src="assets/js/demo.js"></script>

<!--UNTUK MENGIRIM ID FILE ATAU FOLDER -->
<script type="text/javascript">
$(document).ready(function(){
 $(".show-modal").click(function(){ // Click to only happen on announce links
   $("#idfile").val($(this).data('id'));
   $("#title").text("View File : " + $(this).data('title'));
   $("#content").html($(this).data('content'));
   $("#created_by").val($(this).data('creator'));
   var id_creator = $("#created_by").val();
   <!--// UNTUK MENGIRIM ID FILE ATAU FOLDER KE TOMBOL UPDATE FILE -->
   var id = $("#idfile").val();
   $("#file-editor").attr("href", "project/file_editor/"+id);
   $('modal-manipulation-file').modal('show');
   <!--// UNTUK MENGIRIM ID FILE ATAU FOLDER KE TOMBOL UPDATE FILE -->
 });
});
</script>
<!--UNTUK MENGIRIM ID FILE ATAU FOLDER -->

<!-- UNTUK SUMMERNOTE -->
<script type="text/javascript" src="assets-summernote/js/summernote.js"></script>
<script type="text/javascript">
  $(function() {
    $(".summernote-text").summernote();
    dialogsFade: true
  });
</script>
<!-- UNTUK SUMMERNOTE -->
</body>

</html>
