<!DOCTYPE html>
<base href="<?php echo base_url();?>" />
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Digital Report - Easiest way to make a report</title>
    <!-- Favicon-->
    <link rel="icon" href="assets/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="assets/fonts/roboto-css/roboto-fontface.css" rel="stylesheet" type="text/css">
    <link href="assets/material-icons/material-icons.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Preloader Css -->
    <link href="assets/plugins/material-design-preloader/md-preloader.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- JQuery DataTable Css -->
    <link href="assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- JQuery Nestable Css -->
    <link href="assets/plugins/nestable/jquery-nestable.css" rel="stylesheet" />

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="assets/css/themes/all-themes.css" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- Multi Select Css -->
    <link href="assets/plugins/multi-select/css/multi-select.css" rel="stylesheet">

</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo base_url() ?>">Digital Report</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">assignment_late</i>
                        <?php foreach ($show_notification_file as $data_notification_file) {?>
                        <span class="label-count"><?php echo $data_notification_file['total_notification']; ?></span>
                        <?php } ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS FILE UPDATE</li>
                        <li class="body">
                            <ul class="menu">
                              <?php foreach ($show_notification_file as $data_notification_file) {?>
                                <li>
                                    <a href="<?php echo base_url('project/view_notification_file/'.$data_notification_file['id_notification_file']) ?>">
                                        <div class="menu-info">
                                            <h4><?php echo "Nama : ".$data_notification_file['sender_notification_file']; ?></h4>
                                            <h4><?php echo "Update file : ".$data_notification_file['name_file']; ?></h4>
                                            <h4><?php echo "Laporan : ".$data_notification_file['project_title']; ?></h4>
                                            <p>
                                                <i class="material-icons">access_time</i> <?php echo $data_notification_file['date_notification_file']; ?>
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="<?php echo base_url('project/view_all_notification_file') ?>">View All Notifications</a>
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                  </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="assets/images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                  <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('DIGITAL_REPORT_NAME'); ?></div>
                  <div class="email">My Dashboard : USR-ID-<?php echo $this->session->userdata('DIGITAL_REPORT_ID_USERS'); ?></div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">NAVIGASI</li>
                    <li>
                        <a href="<?php echo base_url('') ?>">
                            <i class="material-icons">home</i>
                            <span>Beranda</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('project/my_project') ?>">
                            <i class="material-icons">folder_special</i>
                            <span>Laporanku</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('project/project_submission') ?>">
                            <i class="material-icons">cloud_upload</i>
                            <span>Pengajuan Judul</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('project/my_released_project') ?>">
                            <i class="material-icons">assignment_turned_in</i>
                            <span>Laporan Rilis</span>
                        </a>
                    </li>
                    <li class="header">AKUN</li>
                    <li>
                        <a href="<?php echo base_url('sign/logout') ?>">
                            <i class="material-icons">lock_outline</i>
                            <span>Keluar</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.3
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
      </section>
