
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Example -->
            <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header bg-blue">
                          <h2>
                              Digital Report<small>Laporan-ku</small>
                          </h2>
                          <ul class="header-dropdown m-r--5">
                          <li>
                              <a href="<?php echo base_url('project/choose_project_category') ?>" role="button" class="show-modal" >
                                    <i class="material-icons">note_add</i>
                              </a>
                            </li>
                          </ul>
                      </div>
                        <div class="body">
                          <?php foreach ($show_all_my_project as $data) {?>
                          <div class="card">
                              <div class="header <?php echo "bg-".$data['color'] ?>">
                                  <h2>
                                      Tipe : <?php echo $data['name_category']; ?>
                                  </h2>
                            <ul class="header-dropdown m-r--5">
                                <li>
                                    <div class="demo-google-material-icon">
                                      <?php if ($data['project_id_owner'] == $this->session->userdata['DIGITAL_REPORT_ID_USERS']) { ?>
                                      <a  role="button" class="show-modal" onClick=$("#modal-add-group").modal('show') data-id= <?php echo $data['id_project']; ?> >
                                          <i class="material-icons">person_add</i>
                                      </a>
                                      <?php } ?>
                                    </div>
                                </li>

                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                      <ul class="dropdown-menu pull-right">
                                    <li><a href="<?php echo base_url('project/detail/'.base64_encode($data['id_project'])); ?>" target="_blank" >Buka di tab baru</a></li>
                                          <?php if ($data['project_id_owner'] == $this->session->userdata['DIGITAL_REPORT_ID_USERS']) {?>
                                          <li class="divider"></li>
                                          <li><a role="button" class="show-modal" onclick=$("#modal-update-title").modal('show') data-id= <?php echo $data['id_project']; ?>
                                            data-title= <?php echo $data['project_title']; ?>>Ubah Judul Laporan</a></li>
                                          <li><a href="<?php echo base_url('project/delete_project/'.$data['id_project']) ?>">Hapus Laporan</a></li>
                                          <li class="divider"></li>
                                          <li><a href="<?php echo base_url('project/update_project_to_public/'.$data['id_project']) ?>">Rilis Laporan</a></li>
                                          <?php } else { ?>
                                            <?php } ?>
                                      </ul>
                                </li>
                            </ul>
                              </div>
                              <div class="body">
                                <p class="lead">
                                <a href="<?php echo base_url('project/detail/'.base64_encode($data['id_project'])); ?>"><?php echo $data['project_title']." "; ?></a>
                                </p>
                                <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active">
                                      <a href="#informasi_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">info</i> Informasi Tentang Laporan
                                      </a>
                                  </li>
                                  <li role="presentation">
                                      <a href="#pembimbing_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">accessibility</i> Pembimbing Laporan
                                      </a>
                                  </li>
                                  <li role="presentation">
                                      <a href="#ketua_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">stars</i> Ketua Kelompok
                                      </a>
                                  </li>
                                  <li role="presentation_<?php echo $data['id_project'] ?>">
                                      <a href="#anggota_<?php echo $data['id_project'] ?>" data-toggle="tab">
                                          <i class="material-icons">group</i> Anggota Kelompok
                                      </a>
                                  </li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <!-- INFORMASI LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in active" id="informasi_<?php echo $data['id_project'] ?>">
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5>Status : <?php echo $data['status']; ?></h5></span>
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5>Dibuat : <?php echo $data['project_date_create']; ?></h5></span>
                                  </div>
                                    <!-- INFORMASI LAPORAN -->

                                  <!-- PEMBIMBING LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in " id="pembimbing_<?php echo $data['id_project'] ?>">
                                    <?php foreach ($show_my_guide as $data_guide) {?>
                                    <?php if ($data_guide['id'] == $data['id_project'] ) {?>
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5><?php echo $data_guide['guide']; ?></h5></span>
                                    <?php } ?>
                                    <?php } ?>
                                  </div>
                                 <!-- PEMBIMBING LAPORAN -->

                                  <!-- KETUA LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in " id="ketua_<?php echo $data['id_project'] ?>">
                                    <span class="badge <?php echo "bg-".$data['color'] ?>"><h5><?php echo $data['name']; ?></h5></span>
                                  </div>
                                  <!-- KETUA LAPORAN -->

                                  <!-- ANGGOTA LAPORAN -->
                                  <div role="tabpanel" class="tab-pane fade in " id="anggota_<?php echo $data['id_project'] ?>">
                                  <?php foreach ($show_my_group as $group) {
                                    if ($group['id'] != $data['id_project']) { ?>
                                    <?php } else {
                                      if ($data['project_id_owner'] == $this->session->userdata['DIGITAL_REPORT_ID_USERS']) {?>
                                      <span class="badge bg-white">
                                      <a href="<?php echo base_url('project/delete_group/'.$group['id_group']) ?>">
                                        <button type="button" class="btn btn-danger btn-xs waves-effect">
                                          <i class="material-icons">delete_forever</i>
                                        </button>
                                      </a>
                                    </span>
                                    <?php } ?>
                                    <span class="badge <?php echo "bg-".$data['color'] ?>">
                                      <h5><?php echo $group['nama_kelompok']; ?></h5>
                                    </span>
                                    <?php }} ?>
                                  </div>
                                  <!-- KETUA LAPORAN -->

                              </div>
                              </div>
                          </div>
                          <?php } ?>
                        </div>
                      </div>
                  </div>
              </div>
              <!--row clearfix -->

              <!-- MODAL DIALOG ADD GROUPS PROJECT -->
              <div class="modal fade" id="modal-add-group" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form action="<?php echo base_url('project/add_group'); ?>" method="post">
                          <div class="modal-header">
                              <h4 class="modal-title" id="defaultModalLabel">Tambahkan Anggota Kelompok</h4>
                          </div>
                          <div class="modal-body modal-col-white">
                            <div class="body">
                                    <input type="hidden" class="form-control" name="id_project" id="id_project"/>
                                    <div class="form-group">
                                      <select name="group" class="form-control show-tick" required="" data-live-search="true">
                                          <option value="">Pilih Partnermu</option>
                                          <?php foreach ($show_account_mahasiswa as $data_mahasiswa) {?>
                                          <option value="<?php echo $data_mahasiswa['id_user']; ?>" data-subtext="<?php echo $data_mahasiswa['name_department']." - ".$data_mahasiswa['id_user'] ?>"><?php echo $data_mahasiswa['name']; ?></option>
                                          <?php } ?>
                                      </select>
                                    </div>
                                </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-link waves-effect">TAMBAHKAN</button>
                              <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                          </div>
                    </form>
                      </div>
                  </div>
              </div>
              <!-- MODAL DIALOG ADD GROUPS PROJECT -->

              <!-- MODAL DIALOG UPDATE TITLE PROJECT -->
              <div class="modal fade" id="modal-update-title" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <form action="<?php echo base_url('project/update_project_title'); ?>" method="post">
                          <div class="modal-header">
                              <h4 class="modal-title" id="defaultModalLabel">Form Ubah Judul Laporan</h4>
                          </div>
                          <div class="modal-body modal-col-white">
                            <div class="body">
                                    <input type="hidden" class="form-control" name="id_project" id="id_project"/>
                                    <p id="current_title">
                                    </p>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control"  name="title_name" placeholder="Ubah Judul Laporan menjadi...." id="title_name" autofocus="true" required/>
                                        </div>
                                    </div>
                                </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-link waves-effect">UPDATE</button>
                              <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                          </div>
                    </form>
                      </div>
                  </div>
              </div>
              <!-- MODAL DIALOG UPDATE TITLE PROJECT -->
          </div>
        </div>
    </section>
