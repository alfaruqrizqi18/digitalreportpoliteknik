<section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Data Pengajuan Judul Laporan
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url('project/create_submission') ?>">Buat Pengajuan</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active">
                                  <a href="#pending" data-toggle="tab">
                                      <i class="material-icons">assignment_late</i> PENDING
                                  </a>
                              </li>
                              <li role="presentation">
                                  <a href="#terjawab" data-toggle="tab">
                                      <i class="material-icons">done_all</i> TERJAWAB
                                  </a>
                              </li>
                          </ul>

                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane fade in active" id="pending">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                          <th>Judul Laporan</th>
                                          <th>Deskripsi</th>
                                          <th>Mahasiswa</th>
                                          <th>Dosen</th>
                                          <th>Jawaban</th>
                                          <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Judul Laporan</th>
                                          <th>Deskripsi</th>
                                          <th>Mahasiswa</th>
                                          <th>Dosen</th>
                                          <th>Jawaban</th>
                                          <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($show_my_submission as $data) {?>
                                        <tr>
                                            <td><?php echo $data['project_title']; ?></td>
                                            <td><?php echo $data['contents_submission']; ?></td>
                                            <?php foreach ($show_sender_submission as $data_sender) {?>
                                              <?php if ($data['id_submission'] == $data_sender['id_submission']) {?>
                                                  <td><?php echo $data_sender['sender_submission']; ?></td>
                                              <?php } ?>
                                            <?php } ?>
                                            <td><?php echo $data['receiver_submission']; ?></td>
                                            <td><span class="badge bg-pink"><?php echo $data['answer_submission']; ?></span></td>
                                            <td>
                                              <a href="<?php echo base_url('project/delete_submission/'.$data['id_submission']) ?>">
                                                <button type="button" class="btn btn-danger btn-block waves-effect">
                                                <i class="material-icons">delete</i>
                                                </button>
                                            </a>
                                          </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="terjawab">
                                  <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                      <thead>
                                          <tr>
                                            <th>Judul Laporan</th>
                                            <th>Deskripsi</th>
                                            <th>Mahasiswa</th>
                                            <th>Dosen</th>
                                            <th>Jawaban</th>
                                          </tr>
                                      </thead>
                                      <tfoot>
                                          <tr>
                                            <th>Judul Laporan</th>
                                            <th>Deskripsi</th>
                                            <th>Mahasiswa</th>
                                            <th>Dosen</th>
                                            <th>Jawaban</th>
                                          </tr>
                                      </tfoot>
                                      <tbody>
                                        <?php foreach ($show_my_submission_answered as $data) {?>
                                          <tr>
                                              <td><?php echo $data['project_title']; ?></td>
                                              <td><?php echo $data['contents_submission']; ?></td>
                                              <?php foreach ($show_sender_submission as $data_sender) {?>
                                                <?php if ($data['id_submission'] == $data_sender['id_submission']) {?>
                                                    <td><?php echo $data_sender['sender_submission']; ?></td>
                                                <?php } ?>
                                              <?php } ?>
                                              <td><?php echo $data['receiver_submission']; ?></td>
                                              <?php
                                              if ($data['answer_submission'] == "Di setujui" && $data['status'] == "PUBLIC") {
                                                $background = "badge bg-green";
                                                $status = "Laporan ini telah dirilis";
                                              }else if ($data['answer_submission'] == "Di tolak") {
                                                $background = "badge bg-red";
                                                $status = "Laporan ini telah ditolak";
                                              }else if ($data['answer_submission'] == "Di setujui" && $data['status'] == "PRIVATE") {
                                                $background = "badge bg-orange";
                                                $status = "Laporan ini dalam proses";
                                              }
                                               ?>
                                              <td><span class="<?php echo $background; ?>"><?php echo $data['answer_submission']; ?></span> <br>
                                                <span class="<?php echo $background; ?>"><?php echo $status; ?></span></td>
                                          </tr>
                                          <?php } ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
      </div>
</section>
