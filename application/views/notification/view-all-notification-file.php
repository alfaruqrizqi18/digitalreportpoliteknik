<section class="content">
  <div class="container-fluid">
    <!-- Tabs With Icon Title -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panel Notifikasi File Update - Digital Report
                            </h2>
                            <small>Panel Notifikasi adalah tempat untuk melihat semua notifikasi file yang telah diupdate oleh dosen pembimbing maupun mahasiswa.</small>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#notifikasi1" data-toggle="tab">
                                        <i class="material-icons">assignment_late</i> NOTIFIKASI FILE UPDATE
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#settings_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">settings</i> PENGATURAN
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="notifikasi1">
                                      <?php foreach ($show_all_my_notification_file as $data) {?>
                                        <div class="header">
                                            <h2>
                                                <?php echo "Notifikasi dari : ".$data['sender_notification_file']; ?>
                                            </h2>
                                            <p>Mengupdate file : <a href="<?php echo base_url('project/file_editor/'.$data['id_file']) ?>"><?php echo $data['name_file'] ?></a> dalam laporan : <a href="<?php echo base_url('project/detail/'.base64_encode($data['id_project'])) ?>"><?php echo $data['project_title']; ?></a></p>
                                            <small><?php echo $data['date_notification_file']; ?></small>
                                            <br><br>
                                                <div class="well">
                                                    <?php echo $data['contents_notification_file']; ?>
                                                </div>
                                                <?php if ($data['id_file'] != NULL) {?>
                                                <a href="<?php echo base_url('project/file_editor/'.$data['id_file']) ?>">
                                                <button class="btn bg-cyan waves-effect m-b-15" type="button">
                                                    Menuju ke <?php echo $data['name_file']; ?>
                                                </button>
                                                </a>
                                                <?php }else if($data['id_file'] == NULL) { ?>
                                                  <span class="badge bg-red">Mohon maaf, laporan terkait sudah dihapus.</span>
                                                <?php } ?>
                                                <?php if ($data['status_notification_file'] == 'UNREAD') {?>
                                                <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="settings_with_icon_title">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tabs With Icon Title -->
  </div>
</section>
