<section class="content">
        <div class="container-fluid">
            <!-- Example -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <?php foreach ($show_notification_file_per_id as $data) {?>
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?php echo "Notifikasi dari : ".$data['sender_notification_file']; ?>
                            </h2>
                            <p>Mengupdate file : <a href="<?php echo base_url('project/file_editor/'.$data['id_file']) ?>"><?php echo $data['name_file'] ?></a> dalam laporan : <a href="<?php echo base_url('project/detail/'.base64_encode($data['id_project'])) ?>"><?php echo $data['project_title']; ?></a></p>
                            <small><?php echo $data['date_notification_file']; ?></small>
                            <br><br>
                                <div class="well">
                                    <?php echo $data['contents_notification_file']; ?>
                                </div>
                                <?php if ($data['id_file'] != NULL) {?>
                                <a href="<?php echo base_url('project/file_editor/'.$data['id_file']) ?>">
                                <button class="btn bg-cyan waves-effect m-b-15" type="button">
                                    Menuju ke <?php echo $data['name_file']; ?>
                                </button>
                                </a>
                                <?php }else if($data['id_file'] == NULL) { ?>
                                  <span class="badge bg-red">Mohon maaf, laporan terkait sudah dihapus.</span>
                                <?php } ?>
                                <?php if ($data['status_notification_file'] == 'UNREAD') {?>
                                <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- #END# Example -->
          </div>
    </section>
