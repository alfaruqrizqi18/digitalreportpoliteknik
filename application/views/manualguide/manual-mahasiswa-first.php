<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui bagaimana langkah-langkah dalam menggunakan Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#1. Get Started with Digital Report</h3><hr>
                                        <p class="col-black">
                                            Sebelumnya pastikan kamu sudah mempunyai sebuah judul untuk laporanmu.
                                            Kemudian kamu perlu menekan tombol <a href="<?php echo base_url('project/choose_project_category') ?>" class="btn btn-primary waves-effect" role="button">Mulai Sekarang</a>
                                            untuk beralih ke halaman <a href="<?php echo base_url('project/choose_project_category') ?>">selanjutnya</a>,
                                            yaitu <b>pemilihan kategori laporan</b>.
                                        </p>
                                    </div>
                                        <img src="<?php echo base_url()."assets/images/manual-guide/1.png" ?>">
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#2. Choosing Project Category</h3><hr>
                                        <p class="col-black">
                                            Dalam tahap ini, <b>kamu akan memilih kategori laporan yang ingin kamu buat</b>.
                                            Terdapat 4 kategori laporan yang dapat kamu pilih.
                                        </p>
                                    </div>
                                        <img src="<?php echo base_url()."assets/images/manual-guide/2.png" ?>">
                                    <div class="caption">
                                      <br>
                                        <p class="col-black">
                                            Tekan salah satu tombol yang berada didalam Cardbox kategori laporan, kemudian akan muncul
                                            sebuah <b>pop-up window</b> berwarna putih seperti dibawah ini, yang berisi <b>form-fill</b> untuk kamu isikan judul laporanmu.
                                        </p>
                                    </div>
                                        <img src="<?php echo base_url()."assets/images/manual-guide/3.png" ?>">
                                        <div class="caption">
                                          <br>
                                            <p class="col-black">
                                                Setelah kamu memilih kategori laporan dan mengisikan judul laporanmu,
                                                kamu bisa menekan tombol <b>ENTER</b> atau dengan cara menekan tombol <b>BUAT</b>
                                                pada <b>pop-up window</b> tersebut untuk men-<b>submit</b> judul laporanmu ke dalam <b>Digital Report</b>.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#3. See all My Report</h3><hr>
                                        <p class="col-black">
                                            Setelah berhasil men-<b>submit</b> judul laporan, kamu akan dibawa
                                            ke halaman <a href="<?php echo base_url('project/my_project') ?>" class="btn btn-primary waves-effect" role="button">Laporanku</a>.
                                            Di dalam <a href="<?php echo base_url('project/my_project') ?>" class="btn btn-primary waves-effect" role="button">Laporanku</a> tersebut akan
                                            berisi laporan-laporan yang telah berhasil kamu buat, dan juga laporan-laporan yang terkait dengan kamu (<b><i><u>kamu menjadi salah satu anggota dari laporan Mahasiswa lain.</u></i></b>).
                                        </p>
                                    </div>
                                        <img src="<?php echo base_url()."assets/images/manual-guide/4.png" ?>">
                                        <br>
                                        <div class="caption">
                                          <!--
                                            <h5>#INFORMATION</h5>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Nomor</th>
                                                        <th>Penjelasan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">#1</th>
                                                        <td>Tipe atau kategori laporan.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">#2</th>
                                                        <td>Tombol untuk menambahkan anggota kelompok. <b>(hanya Ketua Kelompok yang bisa menambahkan anggota baru)</b></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">#3</th>
                                                        <td>Tombol Opsi Laporan : berisi fitur <b>Rilis Laporan</b>, <b>Hapus Laporan</b> dan <b>Open New Tab</b>. <b>(hanya Ketua Kelompok yang bisa menggunakan fitur <u><i>Rilis Laporan</i></u> dan <u><i>Hapus Laporan</i></u>)</b></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">#4</th>
                                                        <td>Judul Laporan</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">#5</th>
                                                        <td>Tab yang berisi semua informasi mengenai laporan. Status Laporan, Tanggal Pembuatan, Dosen Pembimbing, Ketua Kelompok dan Anggota Kelompok.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">#6</th>
                                                        <td>Tombol untuk membuat sebuah laporan baru.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                          -->
                                                <p class="col-black">
                                                  Dengan meng-<b>klik</b> judul laporan tersebut, kamu akan dialihkan ke halaman
                                                  <b>Detail Laporan</b> dari laporan yang telah kamu <b>klik</b> tersebut.
                                                </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
