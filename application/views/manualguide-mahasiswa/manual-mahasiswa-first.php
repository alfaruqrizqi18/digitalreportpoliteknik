<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Pembuatan Laporan
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui bagaimana langkah-langkah dalam menggunakan Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#1. Get Started with Digital Report</h3><hr>
                                        <p class="col-black align-justify">
                                            Sebelumnya pastikan kamu sudah mempunyai sebuah judul untuk laporanmu.
                                            Kemudian kamu perlu menekan tombol <a href="<?php echo base_url('project/choose_project_category') ?>" class="btn btn-primary waves-effect" role="button">Mulai Sekarang</a>
                                            untuk beralih ke halaman <a href="<?php echo base_url('project/choose_project_category') ?>">selanjutnya</a>,
                                            yaitu <b>pemilihan kategori laporan</b>.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                            <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/1.png" ?>">
                                          </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#2. Choosing Project Category</h3><hr>
                                        <p class="col-black align-justify">
                                            Dalam tahap ini, <b>kamu akan memilih kategori laporan yang ingin kamu buat</b>.
                                            Terdapat 4 kategori laporan yang dapat kamu pilih. Tekan salah satu tombol yang berada didalam Cardbox kategori laporan, kemudian akan muncul
                                            sebuah <b>pop-up window</b> berwarna putih, yang berisi <b>form-fill</b> untuk kamu isikan judul laporanmu.
                                        </p>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                            <img src="<?php echo base_url()."assets/images/manual-guide/2.1.png" ?>">
                                          </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                            <img src="<?php echo base_url()."assets/images/manual-guide/2.2.png" ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                          <div class="thumbnail">
                                          <img src="<?php echo base_url()."assets/images/manual-guide/2.3.png" ?>">
                                        </div>
                                        </div>
                                        <div class="col-sm-4 col-md-3">
                                            <div class="thumbnail">
                                            <img src="<?php echo base_url()."assets/images/manual-guide/2.4.png" ?>">
                                          </div>
                                        </div>
                                    </div>
                                    </div>
                                        <div class="col-sm-4 col-md-12">
                                        <div class="caption">
                                            <p class="col-black align-justify">
                                                <b>Pop-up window</b> akan muncul seperti dibawah ini.
                                            </p>
                                        </div>
                                        </div>
                                            <div class="thumbnail">
                                            <img src="<?php echo base_url()."assets/images/manual-guide/3.png" ?>">
                                            <div class="caption">
                                                <p class="col-black align-justify">
                                                    Setelah kamu memilih kategori laporan dan mengisikan judul laporanmu,
                                                    kamu bisa menekan tombol <b>ENTER</b> atau dengan cara menekan tombol <b>BUAT</b>
                                                    pada <b>pop-up window</b> tersebut untuk men-<b>submit</b> judul laporanmu ke dalam <b>Digital Report</b>.
                                                </p>
                                            </div>
                                            </div>
                                </div>
                                <div class="col-sm-12 col-md-12">
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#3. See all My Report</h3><hr>
                                        <p class="col-black align-justify">
                                            Setelah berhasil men-<b>submit</b> judul laporan, kamu akan dibawa
                                            ke halaman <a href="<?php echo base_url('project/my_project') ?>" class="btn btn-primary waves-effect" role="button">Laporanku</a>.
                                            Di dalam <a href="<?php echo base_url('project/my_project') ?>" class="btn btn-primary waves-effect" role="button">Laporanku</a> tersebut akan
                                            berisi laporan-laporan yang telah berhasil kamu buat, dan juga laporan-laporan yang terkait dengan kamu (<b><i><u>kamu menjadi salah satu anggota dari laporan Mahasiswa lain.</u></i></b>).
                                        </p>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/4.png" ?>">
                                                <div class="caption">
                                                        <p class="col-black align-justify">
                                                          Di <a href="<?php echo base_url('project/my_project') ?>" class="btn btn-primary waves-effect" role="button">Laporanku</a>
                                                          kamu juga bisa melihat informasi-informasi mengenai laporan yang telah kamu buat. Seperti status laporan,
                                                          tanggal dibuatnya laporan, pembimbing laporan dan ketua kelompok serta anggota kelompok.
                                                        </p>
                                                </div>
                                            </div>
                                      </div>
                                    </div>
                                    </div>
                                    <div class="thumbnail">
                                    <div class="caption">
                                            <p class="col-black align-justify">
                                              Langkah selanjutnya setelah berhasil membuat laporan adalah kamu harus mengajukan
                                              judul tersebut kepada <b>Calon Dosen Pembimbing</b> yang kamu inginkan.
                                              Lanjutkan membaca <b>Buku Panduan</b> ini dengan menekan tombol <b>Baca Panduan selanjutnya</b>
                                              dibawah ini.
                                            </p>
                                    </div>
                                    </div>
                                    <p class="align-right">
                                    <a href="<?php echo base_url('manualguide/secondreport') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan selanjutnya</a>
                                    </p>
                                </div>
                            </div>
                        </div> <!--BODY -->
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
