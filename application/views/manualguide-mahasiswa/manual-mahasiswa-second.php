<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Pembuatan Laporan
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui bagaimana langkah-langkah dalam menggunakan Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide/firstreport') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan sebelumnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#4. Creating a Submission</h3><hr>
                                        <p class="col-black align-justify">
                                          Yang harus kamu lakukan adalah menekan menu <a href="<?php echo base_url('project/project_submission') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Pengajuan Judul</a>
                                          yang berada di sidebar kiri. Seperti gambar dibawah ini.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                        <div class="thumbnail">
                                            <img src="<?php echo base_url()."assets/images/manual-guide/5.png" ?>">
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-4 col-md-12">
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <p class="col-black align-justify">
                                          Kemudian kamu akan dialihkan ke halaman <b>Data Pengajuan Judul Laporan</b>.
                                          Dalam halaman ini kamu dapat mengelola Pengajuan Judul yang ingin kamu buat.
                                          Dalam <b>Data Pengajuan Judul Laporan</b> juga terdapat <b>2 Tab</b> yaitu
                                          <b>Pending</b> dan <b>Terjawab</b>.
                                        </p>
                                    </div>
                                        <img src="<?php echo base_url()."assets/images/manual-guide/6.png" ?>">
                                        <div class="caption">
                                            <h5>#INFORMATION</h5>
                                            <table class="table table-bordered table-hover table-striped" style="black">
                                                <thead>
                                                    <tr>
                                                        <th>Tab</th>
                                                        <th>Penjelasan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">PENDING</th>
                                                        <td>Tab Pending berisi semua Data Pengajuan Judul Laporan yang pernah kamu buat, namun <b>belum mendapatkan jawaban</b> dari dosen terkait.</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">TERJAWAB</th>
                                                        <td>Tab Terjawab berisi semua Data Pengajuan Judul Laporan yang pernah kamu buat dan pengajuan tersebut <b>sudah mendapat jawaban</b> dari dosen terkait.</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <p class="col-black align-justify">
                                              Untuk membuat Pengajuan Judul Laporan baru, klik icon <b>Opsi</b> di <b>Title Bar</b>. Seperti dibawah ini.
                                            </p>
                                        </div>
                                        <div class="thumbnail">
                                          <img src="<?php echo base_url()."assets/images/manual-guide/7.png" ?>">
                                        </div>
                                        <div class="caption">
                                        <p class="col-black align-justify">
                                          Kemudian pilih <b>Buat Pengajuan</b>.
                                        </p>
                                        </div>
                                        <div class="thumbnail">
                                          <img src="<?php echo base_url()."assets/images/manual-guide/8.png" ?>">
                                        </div>
                                        <div class="caption">
                                        <p class="col-black align-justify">
                                          Setelah itu, kamu akan dialihkan ke halaman <b>Form Pengajuan Judul Laporan</b>.
                                        </p>
                                        </div>
                                        <div class="thumbnail">
                                          <img src="<?php echo base_url()."assets/images/manual-guide/9.png" ?>">
                                        </div>
                                        <div class="caption">
                                        <p class="col-black align-justify">
                                          Kemudian kamu bisa memilih <b>Judul Laporan</b> yang ingin kamu ajukan. Pilih Dosen yang ingin
                                          kamu jadi Dosen Pembimbing Laporanmu dan jelaskan secara ringkas tentang laporan yang ingin kamu buat.
                                          Dan selanjutnya kamu harus tekan tombol <b>Kirim Pengajuan</b> atau tekan tombol <b>ENTER</b>.
                                        </p>
                                        </div>
                                    </div>
                                    <p class="align-right">
                                    <a href="<?php echo base_url('manualguide/thirdreport') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan selanjutnya</a>
                                    </p>
                                  </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
