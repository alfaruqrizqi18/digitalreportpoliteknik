<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Tentang Hak Privilege
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui hak-hak privilege di Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#1. Are you a Group Leader ?</h3><hr>
                                        <p class="col-black align-justify">
                                            Jika kamu bertindak sebagai <b>Ketua Kelompok</b>, dapat dipastikan bahwa kamu yang membuat laporan tersebut.
                                            <b>Tugas seorang Ketua Kelompok disini sangat penting</b>, <b>Hak Privilige</b> apa saja yang akan didapat
                                            jika kamu bertindak sebagai <b>Hak Privilige</b> ? Kamu bisa melihat dalam gambar dibawah ini.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.1 Title Bar Icon</h3><hr>
                                                <p class="col-black align-justify">
                                                  Dalam <b>Title Bar</b> kamu akan disediakan <b>1 icon tambahan</b> yang berfungsi sebagai
                                                  penambahan anggota kelompok dalam laporan.
                                                </p>
                                            </div>
                                              <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/19.1.png" ?>">
                                              </div>
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                    <h4>#Notes : </h4>Namun apabila kamu bukan <b>Ketua Kelompok</b>, icon tambahan yang
                                                    berfungsi sebagai penambahan anggota kelompok akan tidak tersedia. Seperti gambar di bawah ini.
                                                  </p>
                                              </div>
                                                <img src="<?php echo base_url()."assets/images/manual-guide/19.2.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.2 Option Menu</h3><hr>
                                                <p class="col-black align-justify">
                                                  Dalam <b>Title Bar</b> icon <b>Opsi</b> juga disediakan <b>3 menu khusus</b> untuk <b>Ketua Kelompok</b>.
                                                  Menu tersebut antara lain adalah <b>Ubah Judul Laporan</b>, <b>Hapus Laporan</b>, <b>Rilis Laporan</b>
                                                </p>
                                            </div>
                                            <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/20.png" ?>">
                                          </div>
                                            <div class="thumbnail">
                                                <div class="caption">
                                                    <p class="col-black align-justify">
                                                      <b>Gambar menu seperti dibawah ini.</b>
                                                    </p>
                                                </div>
                                                <img src="<?php echo base_url()."assets/images/manual-guide/21.1.png" ?>">
                                                <div class="caption">
                                                    <p class="col-black align-justify">
                                                      <h4>#Notes : </h4>Namun apabila kamu bukan <b>Ketua Kelompok</b>, menu-menu dalam
                                                      icon <b>Opsi</b> tersebut hanya menyediakan menu <b>Buka di tab baru</b>. Seperti gambar di bawah ini.
                                                    </p>
                                                </div>
                                                  <img src="<?php echo base_url()."assets/images/manual-guide/21.2.png" ?>">
                                            </div>
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                  <h5>#INFORMATION</h5>
                                                  <table class="table table-bordered table-hover table-striped" style="black">
                                                      <thead>
                                                          <tr>
                                                              <th>Menu</th>
                                                              <th>Penjelasan</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Buka di tab baru</th>
                                                              <td>Untuk membuka secara detail laporan di tab baru pada browser.</td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Ubah Judul Laporan</th>
                                                              <td>Untuk mengganti judul laporan dengan nama laporan baru.</td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Hapus Laporan</th>
                                                              <td>Untuk menghapus laporan.</td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Rilis Laporan</th>
                                                              <td>Digunakan untuk merilis laporan ke <b>Publik</b>, apabila laporan tersebut telah selesai dikerjakan.</td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                                  </p>
                                              </div>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3 Tab Menu</h3><hr>
                                                <p class="col-black align-justify">
                                                  Informasi mengenai laporan yang telah kamu buat, telah disediakan dalam <b>4 tab menu</b> dan masing-masing
                                                  tab menu menyediakan informasi yang berbeda.
                                                </p>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.1 Tab Informasi Tentang Laporan</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Status Laporan</b> dan <b>Tanggal Pembuatan Laporan</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/22.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.2 Tab Pembimbing Tentang Laporan</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Nama dari Dosen Pembimbing Laporanmu</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/23.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.3 Tab Ketua Kelompok</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Nama dari Ketua Kelompokmu</b>.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/24.png" ?>">
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.3.4 Tab Anggota Kelompok</h3>
                                                <p class="col-black align-justify">
                                                  Berisi tentang <b>Nama-nama dari seluruh Anggota Kelompokmu</b>. Dan jika kamu adalah
                                                  seorang <b>Ketua Kelompok</b>, tombol
                                                  <button type="button" class="btn btn-danger btn-xs waves-effect">
                                                    <i class="material-icons">delete_forever</i>
                                                  </button> akan tersedia yang berguna untuk menghapus anggota, dari kelompokmu.
                                                </p>
                                            </div>
                                              <img src="<?php echo base_url()."assets/images/manual-guide/25.png" ?>">
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                    <h4>#Notes : </h4>Namun apabila kamu bukan <b>Ketua Kelompok</b>, tombol
                                                    <button type="button" class="btn btn-danger btn-xs waves-effect">
                                                      <i class="material-icons">delete_forever</i>
                                                    </button> tidak akan tersedia seperti gambar dibawah ini.
                                                  </p>
                                              </div>
                                                <img src="<?php echo base_url()."assets/images/manual-guide/26.png" ?>">
                                            </div>
                                            </div>

                                              <div class="thumbnail">
                                              <div class="caption">
                                                  <h3>#1.4 Option Menu in Detail Report</h3><hr>
                                                  <p class="col-black align-justify">
                                                    Dalam <b>Title Bar</b> di <b>Detail Laporan</b> telah disediakan icon <b>Opsi</b> dengan <b>2 menu </b> yaitu,
                                                    <b>Buat Folder Baru</b> dan <b>Buat File Baru</b> .
                                                  </p>
                                              </div>
                                              <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/27.png" ?>">
                                              </div>
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                    <b>Seperti gambar dibawah ini</b> adalah <b>2 menu</b> yang telah disediakan.
                                                  </p>
                                              </div>
                                              <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/28.1.png" ?>">
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                    <h4>#Notes : </h4>Namun apabila kamu bukan <b>Ketua Kelompok</b>, menu yang tersedia
                                                    adalah <b>Buat File Baru</b> karena hanya <b>Ketua Kelompok</b> yang memiliki privilege ini. Seperti gambar dibawah ini
                                                  </p>
                                              </div>
                                                <img src="<?php echo base_url()."assets/images/manual-guide/28.2.png" ?>">
                                              </div>
                                              <div class="caption">
                                                  <p class="col-black align-justify">
                                                  <h5>#INFORMATION</h5>
                                                  <table class="table table-bordered table-hover table-striped" style="black">
                                                      <thead>
                                                          <tr>
                                                              <th>Menu</th>
                                                              <th>Penjelasan</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Buat Folder Baru</th>
                                                              <td>Digunakan untuk membuat sebuah folder untuk sebagai wadah dari file-file yang ada dilaporan tersebut.
                                                              Folder disini memiliki fungsi yang sama seperti <b>BAB</b> atau <b>Chapter</b></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Buat File Baru</th>
                                                              <td>Digunakan untuk membuat sebuah file baru yang didalam file tersebut nantinya akan berisi data-data dari laporan yang
                                                                akan kamu buat. <b>Kamu tidak akan bisa membuat sebuah file apabila, belum ada satupun folder yang berada didalam laporan tersebut</b></td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                                  </p>
                                              </div>
                                            </div>

                                            <div class="thumbnail">
                                            <div class="caption">
                                                <h3>#1.5 Button in Text Editor</h3><hr>
                                                <p class="col-black align-justify">
                                                  Dalam <b>Title Bar</b> di <b>Text Editor</b> tersedia tombol
                                                  <button type="button" class="btn btn-success waves-effect">
                                                    Kembali
                                                  </button> kemudian disisi kanan <b>Title Bar</b> terdapat tombol
                                                  <button type="button" class="btn btn-danger waves-effect">
                                                    Hapus File ?
                                                  </button> dan tombol terakhir berada disisi bawah text editor, yaitu tombol
                                                  <button type="button" class="btn btn-primary waves-effect">
                                                    Simpan
                                                  </button> seperti gambar dibawah ini.
                                                </p>
                                            </div>
                                            <div class="thumbnail">
                                              <img src="<?php echo base_url()."assets/images/manual-guide/29.1.png" ?>">
                                            </div>
                                            <div class="caption">
                                                <p class="col-black align-justify">
                                                  <h5>#PRIVILEGE-INFORMATION</h5>
                                                  <table class="table table-bordered table-hover table-striped" style="black">
                                                      <thead>
                                                          <tr>
                                                              <th>Login sebagai</th>
                                                              <th>
                                                                <button type="button" class="btn btn-success waves-effect">
                                                                  Kembali
                                                                </button>
                                                              </th>
                                                              <th>
                                                                <button type="button" class="btn btn-danger waves-effect">
                                                                  Hapus File ?
                                                                </button>
                                                              </th>
                                                              <th>
                                                                <button type="button" class="btn btn-primary waves-effect">
                                                                  Simpan
                                                                </button>
                                                              </th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                          <tr>
                                                              <th scope="row">Author File</th>
                                                              <td><span class="label bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Dosen Pembimbing</th>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-red">Tidak Tersedia</span></td>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                          </tr>
                                                          <tr>
                                                              <th scope="row">Tanpa Login</th>
                                                              <td><span class="badge bg-blue">Tersedia</span></td>
                                                              <td><span class="badge bg-red">Tidak Tersedia</span></td>
                                                              <td><span class="badge bg-red">Tidak Tersedia</span></td>
                                                          </tr>
                                                      </tbody>
                                                  </table>
                                                </p>
                                            </div>
                                            <div class="caption">
                                              <p class="col-black align-justify">
                                                <b>Selamat untuk kamu,</b> karena telah membaca panduan ini. Sekarang kamu jadi lebih mengerti tentang <b>Privilege</b> di dalam Digital Report.
                                              </p>
                                            </div>
                                          </div>

                                        </div>
                                    </div>
                                </div>
                                <p class="align-center">
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-link waves-effect" role="button">Baca Panduan sebelumnya</a>
                                <a href="<?php echo base_url('manualguide') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan lainnya di Digital Report</a>
                                </p>
                            </div>
                        </div> <!--BODY -->
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
