<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Pembuatan Laporan
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui bagaimana langkah-langkah dalam menggunakan Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide/secondreport') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan sebelumnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#5. Creating a Folder</h3><hr>
                                        <p class="col-black align-justify">
                                          Jika kamu sudah sampai ke tahap ini, berarti kamu sudah siap untuk membuat sebuah laporan.
                                          Namun sebelumnya, kamu harus membuat folder terlebih dahulu sebagai wadah untuk file-file yang akan kamu buat nanti.
                                          Kamu hanya perlu menekan icon <b>Opsi</b> pada <b>Title Bar</b> seperti dibawah ini.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                            <div class="thumbnail">
                                                <div class="thumbnail">
                                                  <img src="<?php echo base_url()."assets/images/manual-guide/10.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Kemudian pilih <b>Buat Folder Baru</b>.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                  <img src="<?php echo base_url()."assets/images/manual-guide/11.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Akan muncul sebuah <b>pop-up window</b> berwarna putih yang berisi <b>form-fill</b>
                                                        untuk kamu isikan nama folder yang kamu inginkan.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/12.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Tekan tombol <b>BUAT</b> untuk men-<b>submit</b> folder baru yang kamu buat.
                                                        Dan akan terlihat seperti gambar dibawah ini.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/13.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Selamat kamu telah sampai tahap ini dan telah berhasil membuat folder dalam laporanmu.
                                                        Untuk tahap selanjutnya adalah <b>Pembuatan File Baru</b> dalam laporanmu.
                                                        Tekan tombol <b>Baca Panduan selanjutnya</b> untuk membaca panduan tentang <b>Pembuatan File Baru</b>.
                                                      </p>
                                                  </div>
                                        </div>
                                        <p class="align-right">
                                        <a href="<?php echo base_url('manualguide/fourthreport') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan selanjutnya</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> <!--BODY -->
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
