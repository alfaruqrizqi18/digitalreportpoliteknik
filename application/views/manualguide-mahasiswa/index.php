<section class="content">
        <div class="container-fluid">
          <!-- Basic Example -->
          <div class="row clearfix">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                      <div class="header bg-blue">
                          <h2>
                              Digital Report<small>Administrator</small>
                          </h2>
                      </div>
                      <div class="body">
                        <p class="font-50 font-bold align-center">Panduan Digital Report</p>
                          <p class="font-17 font-normal align-center">
                            Panduan Digital Report adalah sebuah panduan yang telah disediakan <b>Digital Report</b> untuk membantu
                            pengguna dalam menggunakan <b>Digital Report</b>. Panduan Digital Report untuk <b>Mahasiswa</b> terbagi menjadi <b>3 jenis</b>, yaitu
                            <b>Panduan Pembuatan Laporan</b>, <b>Tentang Hak Privilege Pengguna</b>, <b>Tentang Navigasi Digital Report</b>.
                          </p>
                            <div class="demo-button">
                              <h1 class="align-center">
                              <a href="<?php echo base_url('manualguide/firstreport') ?>"><button type="button" class="btn btn-lg bg-teal waves-effect"><h4>Panduan Pembuatan Laporan</h4></button></a>
                              <a href="<?php echo base_url('manualguide/privilege') ?>"><button type="button" class="btn btn-lg bg-lime waves-effect"><h4>Tentang Hak Privilege Pengguna</h4></button></a>
                              <a href="<?php echo base_url('manualguide/firstmenu') ?>"><button type="button" class="btn btn-lg bg-deep-orange waves-effect"><h4>Tentang Navigasi Digital Report</h4></button></a>
                            </h1>
                            </div>
                          </div>

                      </div>
                  </div>
            </div>
        </div>
</section>
