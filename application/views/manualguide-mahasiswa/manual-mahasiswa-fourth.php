<section class="content">
        <div class="container-fluid">
          <!-- Custom Content -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                Panduan Pembuatan Laporan
                                <small>Panduan ini akan membantu pengguna yaitu Mahasiswa, agar mengetahui bagaimana langkah-langkah dalam menggunakan Digital Report.</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <p class="align-left">
                                <a href="<?php echo base_url('manualguide/thirdreport') ?>" class="btn btn-primary btn-lg waves-effect" role="button">Baca Panduan sebelumnya</a>
                                </p>
                                    <div class="thumbnail">
                                    <div class="caption">
                                        <h3>#6. Creating a File</h3><hr>
                                        <p class="col-black align-justify">
                                          Setelah kamu berhasil dalam tahap sebelumnya, yaitu membuat <b>Folder</b> sekarang kamu telah memasuki tahap
                                          <b>Pembuatan File Baru</b>. Langkahnya adalah kamu menekan icon <b>Opsi</b> di <b>Title Bar</b> seperti gambar dibawah ini.
                                        </p>
                                        <div class="col-sm-4 col-md-12">
                                            <div class="thumbnail">
                                                <div class="thumbnail">
                                                  <img src="<?php echo base_url()."assets/images/manual-guide/10.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Kemudian pilih <b>Buat File Baru</b>.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                  <img src="<?php echo base_url()."assets/images/manual-guide/14.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Akan muncul sebuah <b>pop-up window</b> berwarna putih yang berisi penentuan di folder mana file tersebut akan disimpan
                                                        dan <b>form-fill</b> untuk kamu isikan nama file yang kamu inginkan.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/15.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Tekan tombol <b>BUAT</b> untuk men-<b>submit</b> file baru yang kamu buat.
                                                        Dan akan terlihat seperti gambar dibawah ini.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/16.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Langkah selanjutnya adalah memanipulasi data yang berada pada file tersebut. Dengan cara meng-<b>klik</b>
                                                        file yang ingin kamu manipulasi datanya, dan akan muncul sebuah <b>pop-up window</b> yang akan memunculkan
                                                        data dari file tersebut. Seperti dibawah ini.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/17.png" ?>">
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Kemudian tekan tombol <a  class="btn btn-success waves-effect" role="button">REVISI</a>
                                                        untuk menuju ke <b>Summernote Text Editor</b> untuk memanipulasi data dari file tersebut. Seperti gambar dibawah ini.
                                                      </p>
                                                  </div>
                                                <div class="thumbnail">
                                                <img src="<?php echo base_url()."assets/images/manual-guide/18.png" ?>">
                                                  <div class="caption">
                                                      <h5>#INFORMATION</h5>
                                                      <table class="table table-bordered table-hover table-striped" style="black">
                                                          <thead>
                                                              <tr>
                                                                  <th>Text Editor</th>
                                                                  <th>Penjelasan</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                                              <tr>
                                                                  <th scope="row">main-text-editor</th>
                                                                  <td>Text Editor ini berfungsi sebagai wadah untuk data dari file terkait.</td>
                                                              </tr>
                                                              <tr>
                                                                  <th scope="row">secondary-text-editor</th>
                                                                  <td>Text Editor ini berfungsi sebagai pesan notifikasi yang akan diterima oleh <b>Dosen Pembimbing</b>
                                                                    laporan terkait, tentang apa saja yang telah kamu <b><u><i>update</i></u></b> dalam file terkait.</td>
                                                              </tr>
                                                          </tbody>
                                                      </table>
                                                  </div>
                                                </div>
                                                  <div class="caption">
                                                      <p class="col-black align-justify">
                                                        Saat kamu di dalam <b>Summernote Text Editor</b>, kamu bisa mengupdate data dari file yang telah kamu
                                                        <a  class="btn btn-success waves-effect" role="button">REVISI</a> dan juga kamu bisa mengirim sebuah pesan notifikasi atau pemberitahuan
                                                        kepada <b>Dosen Pembimbing</b> laporanmu terkait file yang telah kamu update.
                                                      </p>
                                                  </div>
                                        </div>
                                        <p class="align-center">
                                          <a href="<?php echo base_url('manualguide/thirdreport') ?>" class="btn btn-link  waves-effect" role="button">Baca Panduan sebelumnya</a>
                                          <a href="<?php echo base_url('manualguide') ?>" class="btn btn-info  waves-effect" role="button">Baca Panduan lainnya didalam Digital Report</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> <!--BODY -->
                    </div>
                </div>
            </div>
            <!-- #END# Custom Content -->
        </div>
</section>
