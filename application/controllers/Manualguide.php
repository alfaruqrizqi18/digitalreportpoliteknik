<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Manualguide extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('project/project_m');
  }

  function index(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/index');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/index');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

  function firstreport(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/manual-mahasiswa-first');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/manual-dosen');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

  function secondreport(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/manual-mahasiswa-second');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/manual-dosen');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

  function thirdreport(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/manual-mahasiswa-third');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/manual-third');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

  function fourthreport(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/manual-mahasiswa-fourth');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/manual-third');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

  function privilege(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/manual-mahasiswa-privilege');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/manual-dosen-privilege');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

  function firstmenu(){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('manualguide-mahasiswa/manual-mahasiswa-firstmenu');
      $this->load->view('templates-master-mahasiswa/tail');

    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('manualguide-dosen/manual-dosen-firstmenu');
      $this->load->view('templates-master-dosen/tail');

    } else {
      redirect(base_url('sign/login'));
    }
  }

}

 ?>
