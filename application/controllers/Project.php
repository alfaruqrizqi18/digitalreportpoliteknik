<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Project extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('project/project_m');
    $this->load->model('account/account_m');
  	$this->load->helper('url_helper');
  }

  function choose_project_category(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
      redirect('sign/login');
    }else{
    $db['show_all_project_category'] = $this->project_m->show_all_project_category();
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('project/choose-category-project',$db);
    $this->load->view('templates-master-mahasiswa/tail');
    }
  }

  function add_project(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
      redirect('sign/login');
    }else{
      $this->project_m->insert_new_project();
    }
  }
  function add_guide($id_project, $id_submission){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
      redirect('sign/login');
    }else{
      $this->project_m->insert_project_guide($id_project,$id_submission);
    }
  }
  function add_group(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
      redirect('sign/login');
    }else{
      $this->project_m->insert_project_group();
    }
  }
  function add_file_folder(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
      redirect('sign/login');
    }else{
      $this->project_m->insert_project_file_folder();
    }
  }
  function add_submission(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
      redirect('sign/login');
    }else{
      $this->project_m->insert_project_submission();
    }
  }

  function my_project(){
    $db['show_all_my_project'] = $this->project_m->show_all_my_project();
    $db['show_all_my_released_project'] = $this->project_m->show_all_my_released_project();
    $db['show_my_group'] = $this->project_m->show_my_group();
    $db['show_my_guide'] = $this->project_m->show_my_guide();
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

    $db['show_account_mahasiswa'] = $this->account_m->show_account_mahasiswa();
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('mahasiswa/my-project',$db);
    $this->load->view('templates-master-mahasiswa/tail');

    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

    $this->load->view('templates-master-dosen/header',$db);
    $this->load->view('dosen/my-project',$db);
    $this->load->view('templates-master-dosen/tail');
    } else {
      redirect('sign/login');
    }
  }

  function my_released_project(){
    $db['show_all_my_released_project'] = $this->project_m->show_all_my_released_project();
    $db['show_my_group'] = $this->project_m->show_my_group();
    $db['show_my_guide'] = $this->project_m->show_my_guide();
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {
      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('mahasiswa/my-released-project',$db);
      $this->load->view('templates-master-mahasiswa/tail');
    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {
      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('dosen/my-released-project',$db);
      $this->load->view('templates-master-dosen/tail');
    }else {
      redirect('sign/login');
    }
  }

    function project_submission(){
      $db['show_my_submission'] = $this->project_m->show_my_submission();
      $db['show_my_submission_answered'] = $this->project_m->show_my_submission_answered();
      $db['show_sender_submission'] = $this->project_m->show_sender_submission();
      //UNTUK NOTIFIKASI//
      $db['show_notification_file'] = $this->project_m->show_notification_file();
      //UNTUK NOTIFIKASI//

      if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {

      $this->load->view('templates-master-mahasiswa/header',$db);
      $this->load->view('mahasiswa/data-project-submission',$db);
      $this->load->view('templates-master-mahasiswa/tail');

      }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {

      $this->load->view('templates-master-dosen/header',$db);
      $this->load->view('dosen/data-project-submission',$db);
      $this->load->view('templates-master-dosen/tail');
      }
    }

  function detail($id){
    $id = base64_decode($id);
    $db['idprojectparsing'] = $id;
    $db['show_folder'] = $this->project_m->show_folder($id);
    $db['show_file'] = $this->project_m->show_file($id);
    $db['show_project_name_by_id'] = $this->project_m->show_project_name_by_id($id);
    $db['show_my_group_only_for_detail_option'] = $this->project_m->show_my_group_only_for_detail_option($id);
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//

    if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Mahasiswa") {
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('project/project-detail',$db);
    $this->load->view('templates-master-mahasiswa/tail');
    } else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == "Dosen") {
    $this->load->view('templates-master-dosen/header',$db);
    $this->load->view('project/project-detail',$db);
    $this->load->view('templates-master-dosen/tail');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL') == ""){
    $this->load->view('templates-master-public/header');
    $this->load->view('project/project-detail',$db);
    $this->load->view('templates-master-public/tail');
    }
  }


  function file_editor($id){
    $db['show_file_to_update'] = $this->project_m->show_file_to_update($id);
    $db['show_button_save_update'] = $this->project_m->show_button_save_update($id);
    $this->load->view('project/file-editor',$db);
  }

  function update_file_editor_and_insert_notification($id){
    $this->project_m->update_file_editor_and_insert_notification($id);
  }
  function update_project_to_public($id){
    $this->project_m->update_project_to_public($id);
  }
  function update_project_title(){
    $this->project_m->update_project_title();
  }


  function create_submission(){
    $db['show_all_my_project_for_submission'] = $this->project_m->show_all_my_project_for_submission();
    $db['show_account_dosen'] = $this->account_m->show_account_dosen();
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('project/create-submission',$db);
    $this->load->view('templates-master-mahasiswa/tail');
  }

  function view_notification_file($id){
    $db['show_notification_file_per_id'] = $this->project_m->show_notification_file_per_id($id);
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('notification/view-notification-file',$db);
    $this->load->view('templates-master-mahasiswa/tail');
  }
  function view_all_notification_file(){
    $db['show_all_my_notification_file'] = $this->project_m->show_all_my_notification_file();
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('notification/view-all-notification-file',$db);
    $this->load->view('templates-master-mahasiswa/tail');
  }

  function delete_project($id){
    $this->project_m->delete_project($id);
  }
  function delete_guide($id_project, $id_submission){
    $this->project_m->delete_guide($id_project, $id_submission);
  }
  function delete_group($id_group){
    $this->project_m->delete_group($id_group);
  }
  function delete_submission($id_submission){
    $this->project_m->delete_submission($id_submission);
  }
  function delete_file($id_file,$id_project){
    $this->project_m->delete_file($id_file,$id_project);
  }

  //DECLINE UNTUK DOSEN
  function decline_submission($id){
    $this->project_m->decline_submission($id);
  }
  //DECLINE UNTUK DOSEN

}


 ?>
