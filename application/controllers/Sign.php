<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Sign extends CI_Controller{

  public function __construct(){
		parent::__construct();
		$this->load->model('admin/akun_m');
    $this->load->model('login/login_m');
		$this->load->helper('url_helper');
	}

  function register(){
    $this->load->view('login-register/register');
  }
  function login(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
    $this->load->view('login/login');
    }else{
      redirect('dashboard');
    }
  }
  function sign_in(){
    $this->login_m->check_login();
  }
  function logout(){
    $this->session->unset_userdata('DIGITAL_REPORT_USERNAME');
    $this->session->unset_userdata('DIGITAL_REPORT_LEVEL');
    $this->session->unset_userdata('DIGITAL_REPORT_NAME');
    $this->session->unset_userdata('DIGITAL_REPORT_ID_USERS');
    $this->session->unset_userdata('DIGITAL_REPORT_DEPARTMENT');
    session_destroy();
    redirect(base_url());
  }

}


 ?>
