<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Search extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('search/search_m');
    $this->load->model('project/project_m');
  }
  function result(){
    $parameter = $this->input->get('search-field');
    $db['search_result'] = $this->search_m->search_result($parameter);
    $db['parameter'] = $parameter;
    $db['show_my_group'] = $this->project_m->show_my_group();
    $db['show_my_guide'] = $this->project_m->show_my_guide();
    $this->load->view('templates-master-public/header');
    $this->load->view('search/search-result',$db);
    $this->load->view('templates-master-public/tail');
  }
}

 ?>
