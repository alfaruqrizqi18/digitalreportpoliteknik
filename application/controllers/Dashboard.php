<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 *
 */
class Dashboard extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->load->model('project/project_m');
  }

  function index(){
  if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="") {
    $db['show_all_my_released_project'] = $this->project_m->show_all_my_released_project();
    $db['show_my_group'] = $this->project_m->show_my_group();
    $db['show_my_guide'] = $this->project_m->show_my_guide();
    $this->load->view('templates-master-public/header');
    $this->load->view('public/index');
    $this->load->view('templates-master-public/tail');

  }else if($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
    $this->load->view('templates-master-admin/header');
    $this->load->view('admin/index');
    $this->load->view('templates-master-admin/tail');

  }else if($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Dosen"){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//
    $this->load->view('templates-master-dosen/header',$db);
    $this->load->view('dosen/index');
    $this->load->view('templates-master-dosen/tail');

  }else if($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Mahasiswa"){
    //UNTUK NOTIFIKASI//
    $db['show_notification_file'] = $this->project_m->show_notification_file();
    //UNTUK NOTIFIKASI//
    $this->load->view('templates-master-mahasiswa/header',$db);
    $this->load->view('mahasiswa/index');
    $this->load->view('templates-master-mahasiswa/tail');
  }
  }



}


 ?>
