<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Admin extends CI_Controller{

  public function __construct(){
		parent::__construct();
		$this->load->model('admin/akun_m');
    $this->load->model('login/login_m');
		$this->load->helper('url_helper');
	}

  function index(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
      redirect(base_url(''));
    }
  }
  function login(){
    $this->login_m->check_login_admin();
  }

// TENTANG AKUN
  function daftar(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
    $db['tampil_data_department'] = $this->akun_m->tampil_data_department();
    $this->load->view('templates-master-admin/header');
    $this->load->view('admin/daftar-akun-baru',$db);
    $this->load->view('templates-master-admin/tail');
  }
}
  function daftar_akun(){
		$this->akun_m->daftar_akun();
  }
  function data_mahasiswa(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
    $db['data_mahasiswa'] = $this->akun_m->data_mahasiswa();
    $this->load->view('templates-master-admin/header');
    $this->load->view('admin/data-master-mahasiswa',$db);
    $this->load->view('templates-master-admin/tail');
  }
  }
  function data_dosen(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
    $db['data_dosen'] = $this->akun_m->data_dosen();
    $this->load->view('templates-master-admin/header');
    $this->load->view('admin/data-master-dosen',$db);
    $this->load->view('templates-master-admin/tail');
  }
  }
  function data_admin(){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
    $db['data_admin'] = $this->akun_m->data_admin();
    $this->load->view('templates-master-admin/header');
    $this->load->view('admin/data-master-admin',$db);
    $this->load->view('templates-master-admin/tail');
  }
  }
  function form_update($id){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
    $db['data_akun_per_id'] = $this->akun_m->data_akun_per_id($id);
    $db['tampil_data_department'] = $this->akun_m->tampil_data_department();
    $this->load->view('templates-master-admin/header');
    $this->load->view('admin/update-akun',$db);
    $this->load->view('templates-master-admin/tail');
  }
  }
  function update_akun($id){
		$this->akun_m->update_akun($id);
  }
  function hapus_akun($id){
    if ($this->session->userdata('DIGITAL_REPORT_LEVEL')!="Admin") {
      $this->load->view('login/login-admin');
    }else if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Admin"){
      $this->akun_m->hapus_akun($id);

  }
  }
// TENTANG AKUN

}


 ?>
