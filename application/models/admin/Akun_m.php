<?php

/**
 *
 */
class Akun_m extends CI_Model{

  public function __construct(){
		$this->load->database();
    $this->load->helper('url');
	}
  function data_akun_per_id($id){
    $query = $this->db->query(
                  "SELECT * FROM public_account
                   LEFT JOIN department ON public_account.id_department = department.id
                   WHERE id_user = '$id'");
    return $query->result_array();
  }
  function data_mahasiswa(){
    $query = $this->db->query(
                  "SELECT * FROM public_account
                   LEFT JOIN department ON public_account.id_department = department.id
                   WHERE level = 'Mahasiswa'
                   ORDER BY public_account.level ASC");
    return $query->result_array();
  }
  function data_dosen(){
    $query = $this->db->query(
                  "SELECT * FROM public_account
                   LEFT JOIN department ON public_account.id_department = department.id
                   WHERE level = 'Dosen'
                   ORDER BY public_account.level ASC");
    return $query->result_array();
  }
  function data_admin(){
    $query = $this->db->query(
                  "SELECT * FROM public_account
                   LEFT JOIN department ON public_account.id_department = department.id
                   WHERE level = 'Admin'
                   ORDER BY public_account.level ASC");
    return $query->result_array();
  }
  function tampil_data_department(){
    $query = $this->db->query("SELECT * FROM department");
    return $query->result_array();
  }
  function daftar_akun(){
    $level = $_POST['level'];
    $data = array(
      'id_user' => $this->input->post('id_user'),
      'name' => $this->input->post('nama'),
      'gender' => $this->input->post('gender'),
      'id_department' => $this->input->post('jurusan'),
      'username' => $this->input->post('username'),
      'password' =>  $this->input->post('password'),
      'level' => $level
      );
    $this->db->insert('public_account', $data);
    if ($level == 'Mahasiswa') {
    redirect(base_url('admin/data_mahasiswa'));
    }elseif ($level == 'Dosen') {
    redirect(base_url('admin/data_dosen'));
    }elseif ($level == 'Admin') {
    redirect(base_url('admin/data_admin'));
    }
  }
  function update_akun($id){
    $current_level = $_POST['current-level'];
    $level = $_POST['level'];
    $data = array(
      'name' => $this->input->post('nama'),
      'gender' => $this->input->post('gender'),
      'id_department' => $this->input->post('jurusan'),
      'username' => $this->input->post('username'),
      'password' =>  $this->input->post('password'),
      'level' => $level
      );
    $this->db->where('id_user', $id);
    $this->db->update('public_account', $data);
    if ($current_level == 'Mahasiswa') {
    redirect(base_url('admin/data_mahasiswa'));
    }elseif ($current_level == 'Dosen') {
    redirect(base_url('admin/data_dosen'));
    }elseif ($current_level == 'Admin') {
    redirect(base_url('admin/data_admin'));
    }
  }
  public function hapus_akun($id){
		$this->db->delete('public_account', array('id_user' => $id));
  	$this->db->delete('project_file', array('created_by' => $id));
  	$this->db->delete('project_folder', array('created_by' => $id));
  	$this->db->delete('project_group', array('id_account' => $id));
  	$this->db->delete('project_guide', array('id_account' => $id));
  	$this->db->delete('project_master', array('project_id_owner' => $id));
    /////////
  	$this->db->delete('project_notification_file', array('sender_notification_file' => $id));
  	$this->db->delete('project_notification_file', array('receiver_notification_file' => $id));
    /////////
  	$this->db->delete('project_submission', array('sender_submission' => $id));
  	$this->db->delete('project_submission', array('receiver_submission' => $id));
    echo "<script>history.go(-1);</script>";
    /////////
	}

}


 ?>
