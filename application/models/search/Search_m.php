<?php

/**
 *
 */
class Search_m extends CI_Model{

  function __construct(){
    $this->load->database();
  }

  function search_result($parameter){
    $query = $this->db->query
             ("SELECT project_master.id AS id_project, public_account.name,
               project_master.project_id_owner, project_category.id_category,
               project_category.name_category, project_master.project_title,
               project_master.project_date_create, project_master.status, project_category.color,
               project_master.status

               FROM project_master

               LEFT JOIN project_group ON project_master.id = project_group.id_project
               LEFT JOIN project_guide ON project_master.id = project_guide.id_project
               LEFT JOIN public_account ON project_master.project_id_owner = public_account.id_user
               LEFT JOIN project_category ON project_master.project_id_category = project_category.id_category
               LEFT JOIN department ON public_account.id_department = department.id

               WHERE
               project_master.status = 'PUBLIC'
               AND project_master.`project_title` LIKE '%$parameter'

               OR
               project_master.status = 'PUBLIC'
               AND project_master.`project_title` LIKE '$parameter%'

               OR
               project_master.status = 'PUBLIC'
               AND project_master.`project_title` LIKE '%$parameter%'
               ##-----------------------------------------------------##
               OR
               project_master.status = 'PUBLIC'
               AND project_category.`name_category` LIKE '%$parameter'

               OR
               project_master.status = 'PUBLIC'
               AND project_category.`name_category` LIKE '%$parameter%'

               OR
               project_master.status = 'PUBLIC'
               AND project_category.`name_category` LIKE '$parameter%'
               ##-----------------------------------------------------##

      	       OR
               project_master.status = 'PUBLIC'
               AND public_account.`name` LIKE '%$parameter'

      	       OR
               project_master.status = 'PUBLIC'
               AND public_account.`name` LIKE '%$parameter%'

      	       OR
               project_master.status = 'PUBLIC'
               AND public_account.`name` LIKE '$parameter%'
               ##-----------------------------------------------------##

               OR
               project_master.status = 'PUBLIC'
               AND public_account.`username` LIKE '%$parameter'

               OR
               project_master.status = 'PUBLIC'
               AND public_account.`username` LIKE '%$parameter%'

               OR
               project_master.status = 'PUBLIC'
               AND public_account.`username` LIKE '$parameter%'
               ##-----------------------------------------------------##

      	       OR
               project_master.status = 'PUBLIC'
               AND department.`name_department` LIKE '%$parameter'

      	       OR
               project_master.status = 'PUBLIC'
               AND department.`name_department` LIKE '%$parameter%'

      	       OR
               project_master.status = 'PUBLIC'
               AND department.`name_department` LIKE '$parameter%'
               ##-----------------------------------------------------##


               GROUP BY project_master.id
               ORDER BY project_master.id DESC");
    return $query->result_array();
  }
}

 ?>
