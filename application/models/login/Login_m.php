<?php

/**
 *
 */
class Login_m extends CI_Model{

  public function __construct(){
    $this->load->database();
    $this->load->helper('url');
  }

  function check_login(){
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $query = $this->db->query(
               "SELECT id_user, name, username, level, name_department
                FROM public_account
                LEFT JOIN department ON public_account.id_department = department.id
                WHERE BINARY username ='$username'
                AND BINARY password = '$password'
                AND level != 'Admin'
                OR BINARY id_user = '$username'
                AND BINARY password = '$password'
                AND level != 'Admin'");
    if ($query->num_rows() == 1) {
      foreach ($query->result() as $session) {
        $session_data['DIGITAL_REPORT_ID_USERS'] = $session->id_user;
        $session_data['DIGITAL_REPORT_USERNAME'] = $session->username;
        $session_data['DIGITAL_REPORT_NAME'] = $session->name;
        $session_data['DIGITAL_REPORT_LEVEL'] = $session->level;
        $session_data['DIGITAL_REPORT_DEPARTMENT'] = $session->name_department;
        $this->session->set_userdata($session_data);
      }
      if ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Mahasiswa") {
        redirect(base_url(''));
      }elseif ($this->session->userdata('DIGITAL_REPORT_LEVEL')=="Dosen") {
        redirect(base_url(''));
      }
    }else{
      echo "<script>alert('Username tidak ditemukan');history.go(-1);</script>";
    }
  }

  function check_login_admin(){
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $query = $this->db->query(
             "SELECT id_user, name, username, level, name_department
              FROM public_account
              LEFT JOIN department ON public_account.id_department = department.id
              WHERE BINARY username ='$username'
              AND BINARY password = '$password'
              AND level = 'Admin'
              OR BINARY id_user = '$username'
              AND BINARY password = '$password'
              AND level = 'Admin'");
    if ($query->num_rows() == 1) {
      foreach ($query->result() as $session) {
        $session_data['DIGITAL_REPORT_ID_USERS'] = $session->id_user;
        $session_data['DIGITAL_REPORT_USERNAME'] = $session->username;
        $session_data['DIGITAL_REPORT_NAME'] = $session->name;
        $session_data['DIGITAL_REPORT_LEVEL'] = $session->level;
        $session_data['DIGITAL_REPORT_DEPARTMENT'] = $session->department;
        $this->session->set_userdata($session_data);
      }
        redirect(base_url(''));
    }else{
      echo "<script>alert('Anda bukan Admin');history.go(-1);</script>";
    }
  }

}


 ?>
