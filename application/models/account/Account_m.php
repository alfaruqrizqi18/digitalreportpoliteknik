<?php

/**
 *
 */
class Account_m extends CI_Model{

  function __construct(){
    $this->load->database();
  }

  function show_account_dosen(){
    $department = $this->session->userdata('DIGITAL_REPORT_DEPARTMENT');
    $query = $this->db->query(
               "SELECT id_user, name, username, level, name_department
               FROM public_account
               LEFT JOIN department ON public_account.id_department = department.id
               WHERE level = 'Dosen'
               AND name_department = '$department'");
    return $query->result_array();
  }
  function show_account_mahasiswa(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $department = $this->session->userdata('DIGITAL_REPORT_DEPARTMENT');
    $query = $this->db->query(
               "SELECT id_user, name, username, level, name_department
               FROM public_account
               LEFT JOIN department ON public_account.id_department = department.id
               WHERE level = 'Mahasiswa'
               AND name_department = '$department'
               AND id_user != '$id_user'");
    return $query->result_array();
  }
}

 ?>
