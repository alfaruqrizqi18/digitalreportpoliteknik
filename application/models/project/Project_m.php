<?php

/**
 *
 */
class Project_m extends CI_Model{

  function __construct(){
    $this->load->database();
    $this->load->helper('url');
  }

  //SHOWING DATA --- SHOWING DATA ///
  function show_project_name_by_id($id){
    $query = $this->db->query(
             "SELECT * FROM project_master
              LEFT JOIN project_category ON project_master.project_id_category = project_category.id_category
              WHERE id = '$id'");
    return $query->result_array();
  }
  function show_my_group_only_for_detail_option($id){
      $query = $this->db->query(
               "SELECT * FROM project_group
                WHERE id_project = '$id'");
      return $query->result_array();
  }
  function show_all_project_category(){
    $query = $this->db->query(
             "SELECT * FROM project_category");
    return $query->result_array();
  }

  function show_all_my_project(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
              "SELECT project_master.id as id_project, public_account.name,
              project_master.project_id_owner, project_category.id_category,
              project_category.name_category, project_master.project_title,
              project_master.project_date_create, project_master.status, project_category.color, project_master.status
               FROM project_master
               LEFT JOIN project_group on project_master.id = project_group.id_project
               LEFT JOIN project_guide ON project_master.id = project_guide.id_project
               LEFT JOIN public_account ON project_master.project_id_owner = public_account.id_user
               LEFT JOIN project_category ON project_master.project_id_category = project_category.id_category
               WHERE project_master.status = 'PRIVATE'
               AND project_master.project_id_owner = '$id_user'
               OR project_master.status = 'PRIVATE'
               AND project_group.id_account = '$id_user'
               OR project_master.status = 'PRIVATE'
               AND project_guide.id_account = '$id_user'
               GROUP BY project_master.id, project_guide.id_project
               ORDER BY project_master.id DESC");
    return $query->result_array();
  }
  function show_all_my_project_for_submission(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
              "SELECT project_master.id as id_project, public_account.name,
              project_master.project_id_owner, project_guide.id_account, project_category.id_category,
              project_category.name_category, project_master.project_title,
              project_master.project_date_create, project_master.status, project_category.color, project_master.status
               FROM project_master
               LEFT JOIN project_group on project_master.id = project_group.id_project
               LEFT JOIN project_guide ON project_master.id = project_guide.id_project
               LEFT JOIN public_account ON project_master.project_id_owner = public_account.id_user
               LEFT JOIN project_category ON project_master.project_id_category = project_category.id_category
               WHERE project_master.status = 'PRIVATE'
               AND project_master.project_id_owner = '$id_user'
               GROUP BY project_master.id, project_guide.id_project
               ORDER BY project_master.id DESC");
    return $query->result_array();
  }
  function show_my_group(){
      $query = $this->db->query(
               "SELECT project_master.id,project_group.id as id_group,
               project_master.project_id_owner, project_group.id_project,
               project_master.project_title, project_group.id_account,
               public_account.name as nama_kelompok
               FROM project_group
               LEFT JOIN public_account ON public_account.id_user = project_group.id_account
               LEFT JOIN project_master ON project_master.id = project_group.id_project
               WHERE project_master.id = project_group.id_project
               AND project_group.id_account IN (SELECT public_account.id_user FROM public_account)");
      return $query->result_array();
  }
  function show_my_guide(){
      $query = $this->db->query(
              "SELECT project_master.id, project_guide.id_account, public_account.name as guide
               FROM project_guide
               LEFT JOIN public_account ON project_guide.id_account = public_account.id_user
               LEFT JOIN project_master ON project_master.id = project_guide.id_project
               WHERE project_master.id = project_guide.id_project");
      return $query->result_array();
  }
  function show_folder($id){
      $query = $this->db->query(
                  "SELECT id_folder, id_project, name_folder
                   FROM project_folder
                   WHERE id_project = '$id'");
      return $query->result_array();
  }
  function show_file($id){
    $query = $this->db->query(
                "SELECT id_file, name_file, name, contents, created_by, parent, date_create
                 FROM project_file
                 LEFT JOIN public_account ON project_file.created_by = public_account.id_user
                 WHERE id_project = '$id'");
    return $query->result_array();
  }
  function show_file_to_update($id){
    $query = $this->db->query(
                "SELECT id_file, id_project, last_modified_by, created_by, name_file,
                name, contents, parent, date_create,
                (SELECT name from project_file
                 LEFT JOIN public_account ON project_file.last_modified_by = public_account.id_user
                 WHERE id_file = '$id')
                 as last_modifier, last_modified_date

                 FROM project_file
                 LEFT JOIN public_account ON project_file.created_by = public_account.id_user
                 WHERE id_file = '$id'");
    return $query->result_array();
  }
  function show_button_save_update($id){
    $query = $this->db->query(
                "SELECT public_account.name, project_guide.id_account
                 FROM project_guide
                 LEFT JOIN public_account ON project_guide.id_account = public_account.id_user
                 LEFT JOIN project_file ON project_file.id_project = project_guide.id_project
                 LEFT JOIN project_master ON project_master.id = project_guide.id_project
                 WHERE project_file.id_file = '$id'");
    return $query->result_array();
  }
  function show_my_submission(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
                "SELECT id_submission, id_project_submission, project_title, contents_submission,
                 name as receiver_submission, answer_submission
                 FROM project_submission
                 LEFT JOIN project_master ON project_master.id = project_submission.id_project_submission
                 LEFT JOIN public_account ON public_account.id_user = project_submission.receiver_submission
                 WHERE answer_submission = 'Menunggu'
                 AND sender_submission = '$id_user'
                 OR answer_submission = 'Menunggu'
                 AND receiver_submission = '$id_user'
                 ORDER BY id_submission DESC");
    return $query->result_array();
  }
  function show_my_submission_answered(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
                "SELECT id_submission, id_project_submission, project_title, contents_submission,
                 name as receiver_submission, answer_submission,
                 (SELECT COUNT(*) FROM project_submission
                 LEFT JOIN project_master ON project_master.id = project_submission.id_project_submission
                 LEFT JOIN public_account ON public_account.id_user = project_submission.receiver_submission
                 WHERE receiver_submission = '$id_user'
                 AND answer_submission = 'Di tolak'
                 OR receiver_submission = '$id_user'
                 AND answer_submission = 'Di setujui'
                 OR sender_submission = '$id_user'
                 AND answer_submission = 'Di tolak'
                 OR sender_submission = '$id_user'
                 AND answer_submission = 'Di setujui'
                ) as total, status
                 FROM project_submission
                 LEFT JOIN project_master ON project_master.id = project_submission.id_project_submission
                 LEFT JOIN public_account ON public_account.id_user = project_submission.receiver_submission
                 WHERE receiver_submission = '$id_user'
                 AND answer_submission = 'Di tolak'
                 OR receiver_submission = '$id_user'
                 AND answer_submission = 'Di setujui'
                 OR sender_submission = '$id_user'
                 AND answer_submission = 'Di tolak'
                 OR sender_submission = '$id_user'
                 AND answer_submission = 'Di setujui'
                 ORDER BY id_submission DESC");
    return $query->result_array();
  }
  function show_sender_submission(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
                "SELECT id_submission, name as sender_submission FROM project_submission
                 LEFT JOIN public_account ON public_account.id_user = project_submission.sender_submission
                 WHERE sender_submission = '$id_user'
                 OR receiver_submission = '$id_user'");
    return $query->result_array();
  }
  function show_all_my_released_project(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
                "SELECT project_master.id as id_project, public_account.name,
                project_master.project_id_owner, project_guide.id_account, project_category.id_category,
                project_category.name_category, project_master.project_title,
                project_master.project_date_create, project_master.status, project_category.color, project_master.status
                 FROM project_master
                 LEFT JOIN project_group on project_master.id = project_group.id_project
                 LEFT JOIN project_guide ON project_master.id = project_guide.id_project
                 LEFT JOIN public_account ON project_master.project_id_owner = public_account.id_user
                 LEFT JOIN project_category ON project_master.project_id_category = project_category.id_category
                 WHERE project_master.status = 'PUBLIC'
                 AND project_master.project_id_owner = '$id_user'
                 OR project_master.status = 'PUBLIC'
                 AND project_group.id_account = '$id_user'
                 OR project_master.status = 'PUBLIC'
                 AND project_guide.id_account = '$id_user'
                 GROUP BY project_master.id, project_guide.id_project
                 ORDER BY project_master.project_date_released DESC");
    return $query->result_array();
  }

  //////////////////////////////////////////////////////////
  /////////////////////////NOTIFIKASI///////////////////////
  //////////////////////////////////////////////////////////
  function show_notification_file(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
                "SELECT id_notification_file, id_project,  project_notification_file.id_file, project_title, project_notification_file.id_file, name_file,
                 name as sender_notification_file, date_notification_file, contents_notification_file,status_notification_file,
                 (SELECT COUNT(*) FROM project_notification_file WHERE receiver_notification_file = '$id_user' AND status_notification_file = 'UNREAD') as total_notification
                 FROM project_notification_file
                 LEFT JOIN public_account ON public_account.id_user = project_notification_file.sender_notification_file
                 LEFT JOIN project_file ON project_file.id_file = project_notification_file.id_file
                 LEFT JOIN project_master ON project_master.id = project_file.id_project
                 WHERE receiver_notification_file = '$id_user'
                 AND status_notification_file = 'UNREAD'
                 ORDER BY id_notification_file DESC");
    return $query->result_array();
  }
  function show_notification_file_per_id($id){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $data_update_status_notification = array(
      'status_notification_file' => "READ"
      );
    $this->db->where('id_notification_file', $id);
    $this->db->update('project_notification_file', $data_update_status_notification);
    $query = $this->db->query(
                "SELECT id_notification_file, id_project, project_file.id_file, project_title,  name_file,
                 name as sender_notification_file, date_notification_file, contents_notification_file,status_notification_file,
                 (SELECT COUNT(*) FROM project_notification_file WHERE receiver_notification_file = '$id_user' AND status_notification_file = 'READ') as total_notification
                 FROM project_notification_file
                 LEFT JOIN public_account ON public_account.id_user = project_notification_file.sender_notification_file
                 LEFT JOIN project_file ON project_file.id_file = project_notification_file.id_file
                 LEFT JOIN project_master ON project_master.id = project_file.id_project
                 WHERE receiver_notification_file = '$id_user'
                 AND status_notification_file = 'READ'
                 AND id_notification_file = '$id'
                 ORDER BY id_notification_file DESC");
    return $query->result_array();
  }
  function show_all_my_notification_file(){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $query = $this->db->query(
                "SELECT id_notification_file, id_project, project_file.id_file, project_title, name_file,
                 name as sender_notification_file, date_notification_file, contents_notification_file, status_notification_file,
                 (SELECT COUNT(*) FROM project_notification_file WHERE receiver_notification_file = '$id_user' AND status_notification_file = 'UNREAD') as total_notification
                 FROM project_notification_file
                 LEFT JOIN public_account ON public_account.id_user = project_notification_file.sender_notification_file
                 LEFT JOIN project_file ON project_file.id_file = project_notification_file.id_file
                 LEFT JOIN project_master ON project_master.id = project_file.id_project
                 WHERE receiver_notification_file = '$id_user'
                 ORDER BY id_notification_file DESC");
    return $query->result_array();
  }
  //////////////////////////////////////////////////////////
  /////////////////////////NOTIFIKASI///////////////////////
  //////////////////////////////////////////////////////////


  //SHOWING DATA --- SHOWING DATA ///

// INSERTING DATA //
  function insert_new_project(){
    $id_category = $_POST['id_category'];
        $data = array(
          'project_id_category' => $this->input->post('id_category'),
          'project_id_owner' => $this->session->userdata('DIGITAL_REPORT_ID_USERS'),
          'project_title' => $this->input->post('title'),
          'project_date_create' => date('d M Y - h:i a'),
          'status' => "PRIVATE"
          );
    $this->db->insert('project_master', $data);
    redirect(base_url('project/my_project'));
  }

  function insert_project_guide($id_project, $id_submission){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
        $data_project = array(
          'id_project' => $id_project,
          'id_account' => $id_user
          );
        $this->db->insert('project_guide', $data_project);

        $data_submission = array(
          'answer_submission' => "Di setujui"
          );
        $this->db->where('id_submission', $id_submission);
        $this->db->update('project_submission',$data_submission);
        redirect(base_url('project/project_submission'));
  }

  function insert_project_group(){
    $id_project = $this->input->post('id_project');
    $group = $this->input->post('group');
    $query = $this->db->query(
                   "SELECT id_project, id_account
                    FROM project_group
                    WHERE id_project = '$id_project'
                    AND id_account = '$group'");
    if ($query->num_rows() > 0) {
      echo "<script>alert('Maaf akun yang kamu masukkan sudah ada didalam kelompokmu');history.go(-1);</script>";
    }else{
    $data = array(
      'id_project' => $this->input->post('id_project'),
      'id_account' => $this->input->post('group')
      );
    $this->db->insert('project_group', $data);
    echo "<script>history.go(-1);</script>";
    }
  }
  function insert_project_file_folder(){
    $tipe = $this->input->post('tipe');
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    ///ID PROJECT DIENKRIPSI//
    $id_project = $this->input->post('idproject');
    $idproject_encrypt = base64_encode($id_project);
    ///ID PROJECT DIENKRIPSI//
    $folder = $this->input->post('folder');
    $file = $this->input->post('file');
    $dirfolder = $this->input->post('dirfolder');
    $date_create = date('d M Y - h:i a');
    if ($tipe == 'Folder') {
    $data = array(
      'id_project' => $id_project,
      'name_folder' => $folder,
      'date_create' => $date_create,
      'created_by' => $id_user
      );
      $this->db->insert('project_folder', $data);
    }else if ($tipe == 'File') {
      $data = array(
      'id_project' => $id_project,
      'parent' => $dirfolder,
      'name_file' => $file,
      'date_create' => $date_create,
      'created_by' => $id_user,
      'last_modified_date' => $date_create,
      'last_modified_by' => $id_user
      );
      $this->db->insert('project_file', $data);
    }
    redirect(base_url('project/detail/'.$idproject_encrypt));
      //echo "<script>history.go(-1);</script>";
  }

  function insert_project_submission(){
  $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
  $nama_user = $this->session->userdata('nama');
  $project = $this->input->post('project');
  $guide = $this->input->post('guide');
  $contents = $this->input->post('contents');
  $query = $this->db->query(
              "SELECT id_project_submission, sender_submission, receiver_submission
               FROM project_submission
               WHERE id_project_submission = '$project'
               AND sender_submission = '$id_user'
               AND receiver_submission = '$guide'");
    if ($query->num_rows() == 1) {
      echo "<script>alert('Maaf pengajuan laporan ini sudah pernah kamu lakukan kepada dosen pembimbing tersebut.');history.go(-1);</script>";
    }else if($query->num_rows() == 0){
    $data_submission = array(
        'id_project_submission' => $project,
        'contents_submission' => $contents,
        'sender_submission' => $id_user,
        'receiver_submission' => $guide,
        'answer_submission' => "Menunggu"
        );
    $insert = $this->db->insert('project_submission', $data_submission);
      if ($insert) {
        echo "<script>alert('Pengajuan berhasil terkirim');history.go(-2);</script>";
      }else{
        echo "<script>alert('Pengajuan belum berhasil. Coba lagi!');history.go(-1);</script>";
      }
    }
  }

// INSERTING DATA //

// UPDATING FILE EDITOR//
  function update_file_editor_and_insert_notification($id){
    $id_user = $this->session->userdata('DIGITAL_REPORT_ID_USERS');
    $contents = $this->input->post('contents');
    $last_modified_date = date('d M Y - h:i a');
    $created_by = $this->input->post('created_by');
    $last_modified_by = $this->input->post('last_modifier');
    $message = $this->input->post('message');
    $data_file = array(
      'contents' => $contents,
      'last_modified_date' => $last_modified_date,
      'last_modified_by' => $id_user
      );
    $this->db->where('id_file', $id);
    $update = $this->db->update('project_file', $data_file);
      if ($update) {
      echo "<script>alert('Update berhasil');history.go(-2);</script>";
            ////////////////////////MULAI PENGAMBILAN KEPUTUSAN////////////////////////////
              if ($created_by == $id_user) {
                  $query = $this->db->query(
                    "SELECT id_account
                    FROM project_guide
                    LEFT JOIN project_file ON project_file.id_project = project_guide.id_project
                    WHERE project_file.id_file = '$id'");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $data) {
                        $last_modifier = $data->id_account;
                        $data_notification_file = array(
                          'id_file' => $id,
                          'sender_notification_file' => $id_user,
                          'receiver_notification_file' => $last_modifier,
                          'contents_notification_file' => $message,
                          'date_notification_file' => $last_modified_date,
                          'status_notification_file' => "UNREAD"
                        );
                        $insert = $this->db->insert('project_notification_file', $data_notification_file);
                      }
                    }
            ////////////////////////JALAN LAIN PENGAMBILAN KEPUTUSAN////////////////////////////
              } else if ($created_by != $id_user) {
                        $data_notification_file = array(
                          'id_file' => $id,
                          'sender_notification_file' => $id_user,
                          'receiver_notification_file' => $created_by,
                          'contents_notification_file' => $message,
                          'date_notification_file' => $last_modified_date,
                          'status_notification_file' => "UNREAD"
                          );
                  $insert = $this->db->insert('project_notification_file', $data_notification_file);
              }
      } else {
        echo "<script>alert('Update gagal');history.go(-1);</script>";
      }
  }
  function update_project_to_public($id){
    $date_update = date('d M Y - h:i:s a');
    $data = array(
      'project_date_released' => $date_update,
      'status' => "PUBLIC"
      );
    $this->db->where('id', $id);
    $update = $this->db->update('project_master', $data);
    if ($update) {
    echo "<script>alert('Laporan telah berhasil di rilis');history.go(-1);</script>";
    }
  }
// UPDATING FILE EDITOR//
  function update_project_title(){
    $id_project = $_POST['id_project'];
    $title_name = $_POST['title_name'];
    $data = array(
      'project_title' => $title_name
      );
    $this->db->where('id', $id_project);
    $update = $this->db->update('project_master', $data);
    redirect(base_url('project/my_project'));
  }

//DELETING DATA //
  function delete_project($id){
    $this->db->delete('project_master', array('id' => $id));
    $this->db->delete('project_group', array('id_project' => $id));
    $this->db->delete('project_guide', array('id_project' => $id));
    $this->db->delete('project_file', array('id_project' => $id));
    $this->db->delete('project_folder', array('id_project' => $id));
    $this->db->delete('project_submission', array('id_project_submission' => $id));
    redirect(base_url('project/my_project'));
  }
  function delete_guide($id_project, $id_submission){
    $this->db->delete('project_guide',
    array('id_project' => $id_project,
          'id_account' => $this->session->userdata('DIGITAL_REPORT_ID_USERS')
        ));

    $data_submission = array(
      'answer_submission' => "Menunggu"
      );
    $this->db->where('id_submission', $id_submission);
    $this->db->update('project_submission',$data_submission);
    redirect(base_url('project/project_submission'));
  }
  function delete_group($id_group){
    $this->db->delete('project_group',
    array('id' => $id_group
        ));
    redirect(base_url('project/my_project'));
  }

  function delete_submission($id_submission){
    $this->db->delete('project_submission',
    array('id_submission' => $id_submission
          ));
    redirect(base_url('project/project_submission'));
  }

  function delete_file($id_file,$id_project){
    $this->db->delete('project_file',
    array('id_file' => $id_file
          ));
    redirect(base_url('project/detail/'.$id_project));
  }

  function decline_submission($id){
      $data_submission = array(
          'answer_submission' => "Di tolak"
          );
      $this->db->where('id_submission', $id);
      $this->db->update('project_submission', $data_submission);
      redirect(base_url('project/project_submission'));
  }
//DELETING DATA //
}

 ?>
